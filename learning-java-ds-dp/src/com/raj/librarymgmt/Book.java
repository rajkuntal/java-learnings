package com.raj.librarymgmt;

public class Book {
	
	private int id;
	
	private String name;
	
	private boolean isIssued;
	
	private User user;
	
	public Book(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isIssued() {
		return isIssued;
	}
	public void setIssued(boolean isIssued) {
		this.isIssued = isIssued;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	public String toString(){
		if(this.user != null)
			return ("Id : "+this.id+", name :"+this.name+", UserId : "+this.user.getName());
		else
			return ("Id : "+this.id+", name :"+this.name);
	}

}
