package com.raj.librarymgmt;

import java.util.List;

public interface BookDao {
	
	public void addBook(String name);
	
	public int fetchBookId(String name);
	
	public boolean issueBook(int id, User user);
	
	public boolean releaseBook(int id);
	
	public List<Book> getAllBooks();

}
