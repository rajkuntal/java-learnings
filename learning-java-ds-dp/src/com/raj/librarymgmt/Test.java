package com.raj.librarymgmt;

public class Test {
	
	public static void main(String[] args) {
		BookDao bookDao = BookDaoImpl.getInstence();
		bookDao.addBook("DSA");
		bookDao.addBook("DSA1");
		bookDao.addBook("DSA2");
		bookDao.addBook("DSA3");
		
		User user = new User();
		user.setUserId(778);
		user.setName("RAJ");
		
		bookDao.issueBook(bookDao.fetchBookId("DSA"), user);
		
		System.out.println(bookDao.getAllBooks());
	}

}
