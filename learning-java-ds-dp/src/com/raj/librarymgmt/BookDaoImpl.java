package com.raj.librarymgmt;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

public class BookDaoImpl implements BookDao {
	
	private static BookDaoImpl instence = null;
	private DataStore store;
	
	private BookDaoImpl(){
		this.store = DataStore.getInstence();
	}
	
	public static BookDaoImpl getInstence(){
		if(instence == null) {
			instence = new BookDaoImpl();
		}
		return instence;
	}

	@Override
	public void addBook(String name) {
		Book book = new Book(store.getBookCount(), name);
		store.addBook(book.getId(), book);
		//increase the book count
		store.increaseBookCount();
		store.setBookMapping(book.getId(), name);
	}

	@Override
	public int fetchBookId(String name) {
		return store.getBookId(name);
	}

	@Override
	public boolean issueBook(int id, User user) {
		if(store.getBooks().containsKey(id)) {
			Book book = store.getBooks().get(id);
			book.setIssued(true);
			book.setUser(user);
			return true;
		}
		return false;
	}

	@Override
	public boolean releaseBook(int id) {
		if(store.getBooks().containsKey(id)) {
			Book book = store.getBooks().get(id);
			book.setIssued(false);
			book.setUser(null);
			return true;
		}
		return false;
	}
	
	public List<Book> getAllBooks(){
		Set<Entry<Integer, Book>> bookSet = store.getBooks().entrySet();
		Iterator<Entry<Integer, Book>> it = bookSet.iterator();
		List<Book> books = new ArrayList<>();
		while(it.hasNext()) {
			Entry<Integer, Book> entry = it.next();
			books.add(entry.getValue());
		}
		return books;
	}

}
