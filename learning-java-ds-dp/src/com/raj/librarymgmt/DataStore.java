package com.raj.librarymgmt;

import java.util.HashMap;
import java.util.Map;

public class DataStore {
	
	private static DataStore instence = null;
	
	private Map<Integer, Book> books;
	
	private Map<Integer, User> users;
	
	private int bookCount;
	
	private int userCount;
	
	private Map<String, Integer> bookIdMapping;
	
	private DataStore() {
		books = new HashMap<>();
		users = new HashMap<>();
		bookCount = 1;
		userCount = 1;
		bookIdMapping = new HashMap<>();
	}
	
	public static DataStore getInstence() {
		if(instence == null) {
			instence = new DataStore();
		}
		
		return instence;
	}
	
	public Map<Integer, Book> getBooks() {
		return books;
	}

	public Map<Integer, User> getUsers() {
		return users;
	}

	public int getBookCount() {
		return bookCount;
	}

	public int getUserCount() {
		return userCount;
	}


	public void addBook(int bookId, Book book) {
		this.books.put(bookId, book);
	}

	public void addUser(int id, User user) {
		this.users.put(id, user);
	}

	public void increaseBookCount() {
		this.bookCount = this.bookCount +1;
	}

	public void increaseUserCount() {
		this.userCount = this.userCount + 1;
	}
	
	public int getBookId(String name){
		return this.bookIdMapping.get("name");
	}
	
	public void setBookMapping(int id, String name){
		this.bookIdMapping.put(name, id);
	}
	
}
