package com.raj.hackerrank.tests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MoblileIron {

	public static void main(String[] args) {

		int[] a = {6, 5, 1, 3, 4, 6, 2};
		finalPrice(a);
	}

	public static void finalPrice(int[] prices) {
		Item[] itmes = new Item[prices[0]];
		for (int i = 1; i <= prices[0]; i++) {
			itmes[i - 1] = new Item(prices[i], i - 1);
		}
		Arrays.sort(itmes);
		int totalCost = 0;
		List<Integer> nonDiscItems = new ArrayList<Integer>();
		for (int i = 1; i <= prices[0]; i++) {
			int disIndex = bstSearch(new Item(prices[i], i), itmes);
			if (disIndex > -1) {
				totalCost = totalCost + prices[i] - prices[disIndex + 1];
			} else {
				totalCost = totalCost + prices[i];
				nonDiscItems.add(i - 1);
			}
		}
		System.out.println(totalCost);
		for (Integer v : nonDiscItems) {
			System.out.print(v + " ");
		}
	}

	public static int bstSearch(Item item, Item[] items) {
		if (item.value > items[items.length - 1].value)
			return -1;
		int mid = (items.length) / 2;
		int last = items.length - 1;
		int first = 0;
		int index = -1;
		while (first <= last) {
			if (item.value >= items[mid].value && (item.index - 1) < items[mid].index) {
				index = items[mid].index;
			}
			if (first == last)
				break;
			last = mid;
			mid = mid / 2;
		}
		return index;
	};

	public static class Item implements Comparable<Item> {
		int value;
		int index;

		public Item(int v, int i) {
			this.value = v;
			this.index = i;
		}
		@Override
		public int compareTo(Item o) {
			if (this.value == o.value)
				return 0;
			if (this.value > o.value)
				return 1;
			return -1;
		}
	}

}
