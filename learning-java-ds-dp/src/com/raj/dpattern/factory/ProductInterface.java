package com.raj.dpattern.factory;

public interface ProductInterface {
	
	public void makeProduct();

}
