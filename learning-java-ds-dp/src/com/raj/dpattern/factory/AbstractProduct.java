package com.raj.dpattern.factory;

public class AbstractProduct {
	
	public String name;
	public String brand;
	
	public void display(){
		System.out.println(this.getClass().getName());
	}
	
}
