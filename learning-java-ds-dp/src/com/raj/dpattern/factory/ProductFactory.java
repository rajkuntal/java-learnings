package com.raj.dpattern.factory;

public class ProductFactory {
	
	public static final String A = "A";
	public static final String B = "B";

	public static ProductInterface getProduct(String s) {
		ProductInterface pro = null;
		switch (s) {
		case A:
			pro = new ProductA();
			break;
		case B:
			pro = new ProductB();
			break;
		}
		return pro;
	}

}
