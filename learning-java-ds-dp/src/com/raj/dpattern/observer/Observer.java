package com.raj.dpattern.observer;

public interface Observer {
	
	public void update();
}
