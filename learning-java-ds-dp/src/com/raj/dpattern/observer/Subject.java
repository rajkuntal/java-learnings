package com.raj.dpattern.observer;

public interface Subject {
	
	public void resisterObserver(Observer observer);
	
	public void deResisterObserver(Observer observer);
	
	public void notofyObservers();
	

}
