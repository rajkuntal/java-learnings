package com.raj.dpattern.observer;

public class Test {
	
	public static void main(String[] args) {
		Publiser publiser = new Publiser();
		
		Observer obsr1= new MessageObserver("Obsr1", publiser);
		Observer obsr2= new MessageObserver("Obsr2", publiser);
		Observer obsr3= new MessageObserver("Obsr3", publiser);
		
		publiser.resisterObserver(obsr1);
		publiser.resisterObserver(obsr2);
		publiser.resisterObserver(obsr3);
		
		publiser.setMessage("hello");
		publiser.setMessage("hello-hello");
	}

}
