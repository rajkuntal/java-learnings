package com.raj.dpattern.observer;

import java.util.ArrayList;
import java.util.List;

public class Publiser implements Subject {
	
	private List<Observer> observers;
	private String message;
	
	public Publiser() {
		this.observers = new ArrayList<>();
	}

	@Override
	public void resisterObserver(Observer observer) {
		if(observer != null)
			this.observers.add(observer);
	}

	@Override
	public void deResisterObserver(Observer observer) {
		if(observer != null) {
			int i = this.observers.indexOf(observer);
			this.observers.remove(i);
		}
	}

	@Override
	public void notofyObservers() {
		for(Observer observer : this.observers){
			observer.update();
		}
	}
	
	public void setMessage(String m) {
		this.message = m;
		notofyObservers();
	}
	
	public String getMessage(){
		return this.message;
	}

}
