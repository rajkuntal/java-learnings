package com.raj.dpattern.observer;

public class MessageObserver implements Observer {
	
	private Publiser publiser;
	private String name;
	
	public MessageObserver(String name, Publiser publiser) {
		this.publiser = publiser;
		this.name = name;
	}

	@Override
	public void update() {
		System.out.println(this.name + ": consuming message - "+this.publiser.getMessage());
	}

}
