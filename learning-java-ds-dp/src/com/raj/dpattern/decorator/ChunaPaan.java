package com.raj.dpattern.decorator;

public class ChunaPaan implements PaanDecorator {
	
	public Paan paan;

	public ChunaPaan(Paan paan) {
		this.paan = paan;
		addSupari();
		System.out.println(paan);
	}
	
	@Override
	public void addSupari() {
		paan.desc = paan.desc + "Adding Supari";
	}
	
	

}
