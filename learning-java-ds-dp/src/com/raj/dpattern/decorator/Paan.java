package com.raj.dpattern.decorator;

public class Paan {
	
	public String desc;
	
	public Paan() {
		this.desc = "Simple Paan";
		System.out.println(this);
	}
	
	@Override
	public String toString() {
		return desc;
	}

}
