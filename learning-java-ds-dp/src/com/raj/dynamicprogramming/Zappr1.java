package com.raj.dynamicprogramming;

public class Zappr1 {
	
	public static void main(String[] args) {
		String[] tests = {"1","6"};
		whoWonTheGame(tests);
	}
	
	static void whoWonTheGame(String[] tests) {
		String player1 = "Clark";
		String player2 = "Bruce";
		boolean turn = true;
		for(int i = 1; i <= Long.valueOf(tests[0]); i++){
			Long totalBalls = Long.valueOf(tests[i]);
			if(getWinner(totalBalls, turn))
				System.out.println(player1);
			else
				System.out.println(player2);
		}
    }
	
	static boolean getWinner(Long totalBalls, boolean turn){
		if(totalBalls == 1)
			return turn;
		long remaingBalls = totalBalls;
		while(remaingBalls >= 1){
			long l = 2;
			if(isPowerOfTwo(remaingBalls)){
				while(l < remaingBalls){
					l = l*2;
				}
			} else {
				l = remaingBalls/2;
			}
			remaingBalls = remaingBalls - l;
			if(remaingBalls >= 1 && turn)
				turn = false;
			else if(remaingBalls >= 1 && !turn)
				turn = true;
			else
				break;
		}
		return turn;
	}
	
	static boolean isPowerOfTwo(Long x)
	{
	    return (x != 0) && ((x & (x - 1)) == 0);
	}

}
