package com.raj.dynamicprogramming;

public class EditDistence {
	
	/*
	 * Given two strings str1 and str2 and below operations that can performed on str1. 
	 * Find minimum number of edits (operations) required to convert ‘str1’ into ‘str2’.
		Insert
		Remove
		Replace
	  Example:
	  	Input:   str1 = "geek", str2 = "gesek"
		Output:  1
			We can convert str1 into str2 by inserting a 's'.
	 */
	
	public static void main(String[] args) {
		String s1 = "dnqaurlp";
		String s2 = "lofnrtmh";
		System.out.println("No of operations : "+editDistence(s1.toCharArray(), s2.toCharArray(), 0, 0));
		String[] ss1 = {"dnqaurlp", "geek"};
		String[] ss2 = {"lofnrtmh", "geej"};
		getMinimumDifference(ss1, ss2).toString();
	}
	
	static int[] getMinimumDifference(String[] a, String[] b) {
		int[] output = new int[a.length];
		if(a.length != b.length)
			return output;
		for(int i = 0; i < a.length; i++){
			System.out.print(editDistence(a[i].toCharArray(), b[i].toCharArray(), 0, 0) + " : ");
			output[i] = editDistence(a[i].toCharArray(), b[i].toCharArray(), 0, 0);
		}
		for(int i = 0; i < a.length; i++){
			System.out.print(output[i] + ",");
		}
		return output;
    }
	
	static int editDistence(char[] s1,  char[] s2, int i, int j) {
		if(s1.length != s2.length) return -1;
		if(i == s1.length) return s2.length-j;
		if(s2.length == j) return s1.length-i;
		
		if(s1[i] == s2[j])
			return editDistence(s1, s2, i+1, j+1);
		return 1+Math.min(editDistence(s1, s2, i+1, j)
							, Math.min(editDistence(s1, s2, i, j+1), editDistence(s1, s2, i+1, j+1))
						 );
	}

}
