package com.raj.dynamicprogramming;

/**
 * 
 * 		Given a character matrix where every cell has one of the following values.

		'C' -->  This cell has coin
		
		'#' -->  This cell is a blocking cell. 
		         We can not go anywhere from this.
		
		'E' -->  This cell is empty. We don't get
		         a coin, but we can move from here.  
		Initial position is cell (0, 0) and initial direction is right.
		
		Following are rules for movements across cells.
		
		If face is Right, then we can move to below cells
		
		Move one step ahead, i.e., cell (i, j+1) and direction remains right.
		Move one step down and face left, i.e., cell (i+1, j) and direction becomes left.
		If face is Left, then we can move to below cells
		
		Move one step ahead, i.e., cell (i, j-1) and direction remains left.
		Move one step down and face right, i.e., cell (i+1, j) and direction becomes right.
		Final position can be anywhere and final direction can also be anything. The target is to collect maximum coins.
 *
 */

public class CollectCoins {
	
	public static void main(String[] args) {
		
		String[][] matrix = { {"E", "C", "C", "C", "C"},
							{"C", "#", "C", "#", "E"},
							{"#", "C", "C", "#", "C"},
							{"C", "E", "#", "C", "E"},
							{"C", "E", "C", "C", "E"},
						};
		
		int coins = coinCollection(matrix, 0, 0, 0, 0, 4, 4, true);
		System.out.println(coins);
	}
	
	public static int coinCollection(String[][] matrix, int i, int j, int currentCoins, int maxCoins, int m, int n, boolean rightFace){
		if(!validCell(i, j, m, n) || "#".equals(matrix[i][j])){
			return maxCoins;
		}
		if("C".equals(matrix[i][j])){
			currentCoins = currentCoins + 1;
		}
		maxCoins = coinComparision(currentCoins, maxCoins);
		if(rightFace){
			maxCoins = coinCollection(matrix, i, j+1, currentCoins, maxCoins, m, n, true);
			maxCoins = coinCollection(matrix, i+1, j, currentCoins, maxCoins, m, n, false);
		} else {
			maxCoins = coinCollection(matrix, i, j-1, currentCoins, maxCoins, m, n, false);
			maxCoins = coinCollection(matrix, i+1, j, currentCoins, maxCoins, m, n, true);
		}
		return maxCoins;
	}
	
	public static boolean validCell(int i, int j, int m, int n){
		if(i>=0 && i<=m && j>=0 && j<=n)
			return true;
		return false;
	}
	
	public static int coinComparision(int current, int max){
		return (current >= max ? current : max);
	}

}
