package com.raj.dynamicprogramming;

public class MaxSumOfNonAdjacentTwoElements {
	
	public static int maxSum(int[] a){
		int maxSum = 0;
		for(int i=0; i<a.length-2;i++){
			for(int j = 0; j<a.length; j++){
				if(j-1>1) {
					if(a[i]+a[j]>maxSum)
						maxSum = a[i]+a[j];
				}
			}
		}
		return maxSum;
	}
	
	public static void main(String[] args) {
		int[] a = {500,160,9,76,-8,600,89};
		System.out.println(maxSum(a));
	}
	
}
