package com.raj.dynamicprogramming;

import java.util.ArrayList;
import java.util.List;

public class LongestIncreasingSubSequence {
	
	/*
	 * The Longest Increasing Subsequence (LIS) problem is to find the length of the longest subsequence of 
	 * a given sequence such that all elements of the subsequence are sorted in increasing order. 
	 * For example, the length of LIS for {10, 22, 9, 33, 21, 50, 41, 60, 80} 
	 * is 6 and LIS is {10, 22, 33, 50, 60, 80}.
	 */
	
	public static void main(String[] args) {
		int[] a = {10, 22, 9, 33, 21, 50, 41, 60, 80};
		System.out.println("Longest Increasing Sub Sequence is : "+subSequence(a));
	}
	
	public static List<Integer> subSequence(int[] a){
		List<Integer> list = new ArrayList<>();
		for(int i=0; i<a.length;i++){
			List<Integer> ll = new ArrayList<Integer>();
			ll.add(a[i]);
			List<Integer> l = findLongestSubSequence(a, a.length, i, a[i], ll);
			if(l.size() > list.size()) {
				list.clear();
				list.addAll(l);
			}
		}
		return list;
	}
	
	private static List<Integer> findLongestSubSequence(int[] a, int len, int i, int element, List<Integer> list) {
		if(i<len-1) {
			if(a[i+1]>element) {
				list.add(a[i+1]);
				return findLongestSubSequence(a, len, i+1, a[i+1], list);
			}
			else
				return findLongestSubSequence(a, len, i+1, element,list);
		}
		return list;
	}

}
