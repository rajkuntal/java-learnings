package com.raj.java.hashing;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class TestHashMap {
	
	public static void main(String[] args) {
		Map<Integer, String> map = new HashMap<>();
		
		map.put(1, "Raj");
		map.put(2, "Sanju");
		
		Set<Entry<Integer, String>> entries = map.entrySet();
		Iterator<Entry<Integer, String>> it = entries.iterator();
		while(it.hasNext()){
			Entry<Integer, String> e = it.next();
			System.out.println("Key: "+e.getKey()+"- Value :"+e.getValue());
		}
		
		// Put New Value with the same key
		System.out.println();
		System.out.println("After putting new value for key - 1");
		map.put(1,"Harish");
		Iterator<Entry<Integer, String>> it1 = entries.iterator();
		while(it1.hasNext()){
			Entry<Integer, String> e = it1.next();
			System.out.println("Key: "+e.getKey()+"- Value :"+e.getValue());
		}
		
		System.out.println();
		System.out.println("Try hash map with employees");
		
		Map<Employee, String> empMap = new HashMap<>();
		empMap.put(new Employee(2, 200, "Raj"), "Raj");
		empMap.put(new Employee(9, 200, "Sanju"), "Sanju");
		empMap.put(new Employee(15, 200, "Harish"), "Harish");
		
		Set<Entry<Employee, String>> empEntries = empMap.entrySet();
		Iterator<Entry<Employee, String>> empIt = empEntries.iterator();
		while(empIt.hasNext()){
			Entry<Employee, String> e = empIt.next();
			System.out.println("Key: "+e.getKey()+" --> Value :"+e.getValue());
		}
		
		System.out.println();
		System.out.println("Add new employee with id 4 then check the value for bucket 1");
		Employee eND = new Employee(4, 200, "ND");
		empMap.put(eND, "ND");
		
		Iterator<Entry<Employee, String>> empIt1 = empEntries.iterator();
		while(empIt1.hasNext()){
			Entry<Employee, String> e = empIt1.next();
			System.out.println("Key: "+e.getKey()+" --> Value :"+e.getValue());
		}
		
		String emp1 = empMap.get(eND);
		
		System.out.println(emp1);
		
		System.out.println();
		System.out.println("Custom Hash Map");
		
		CustomHashMap customHashMap = new CustomHashMap();
		ContainerObject cObj = new ContainerObject(1, "Raj");
		customHashMap.put(cObj, "Raj");
		customHashMap.put(new ContainerObject(6, "Sanju"), "Sanju");
		customHashMap.put(new ContainerObject(3, "ND"), "ND");
		customHashMap.put(new ContainerObject(10, "Harish"), "Harish");
		
		for(int i = 0; i < customHashMap.map.size(); i++) {
			ContainerObject obj = customHashMap.map.get(i);
			while(obj != null) {
				System.out.println(obj);
				obj = obj.next;
			}
		
		}
		
		System.out.println();
		System.out.println("Find in Custom Hash Map");
		System.out.println(customHashMap.get(cObj));
	}

}
