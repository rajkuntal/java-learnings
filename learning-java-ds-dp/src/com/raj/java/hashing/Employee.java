package com.raj.java.hashing;

public class Employee {
	
	public int id;
	public int salary;
	public String name;
	
	public Employee(int id, int sal, String name){
		this.id = id;
		this.salary = sal;
		this.name = name;
	}
	
	@Override
	public int hashCode() {
		if(id < 5)
			return 5;
		else if(id >= 5 && id < 10)
			return 10;
		else
			return id;
	}
	
	public boolean equals(Employee obj) {
		if(this.name.equals(obj.name))
			return true;
		else
			return false;
	};
	
	@Override
	public String toString() {
		return ("id : "+this.id+", name : "+this.name+", Salary : "+this.salary);
	}

}
