package com.raj.java.hashing.myhashmap;

public class HashMap<K,V> {
	
	public Entry<K,V>[] buckets = null;
	
	public HashMap(int size){
		buckets = new Entry[size];
	}
	
	public boolean put(K key, V value){
		if(!this.contains(key)){
			Entry<K,V> entry = new Entry<K,V>(key, value);
			buckets[key.hashCode()] = entry;
		}
		return true;
	}
	
	public boolean contains(Object key){
		if(buckets[key.hashCode()] != null)
			return true;
		return false;
	}
	
}
