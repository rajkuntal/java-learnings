package com.raj.java.hashing.myhashmap;

import java.util.Set;

public class Test {
	
	public static void main(String[] args) {
		
		HashMap<String, Employee> map = new HashMap<String, Employee>(10);
		map.put("", new Employee(23, "Raj"));
		System.out.println("Hello");
		
		java.util.HashMap<Employee, Employee> emps = new java.util.HashMap<>();
		Employee emp1 = new Employee(23, "Raj");
		Employee emp2 = new Employee(33, "Naj");
		emps.put(emp1, emp1);
		emps.put(emp2, emp2);
		Set<Employee> keys = emps.keySet();
		
		java.util.HashMap<Integer, Integer> map2 = new java.util.HashMap<>();
		map2.put(488, 2);
		map2.put(343, 7);
		map2.put(43, 9);
		System.out.println();
		
		java.util.HashMap<Integer, Integer> map3 = new java.util.HashMap<>();
		map3.put(488, 2);
		map3.put(343, 7);
		map3.put(43, 9);
		System.out.println();
	}

}
