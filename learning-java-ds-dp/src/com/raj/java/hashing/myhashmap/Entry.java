package com.raj.java.hashing.myhashmap;

public class Entry<K,V> {
	public K key;
	public V value;
	public Entry<K,V> entry;
	
	public Entry(K key, V value){
		this.key = key;
		this.value = value;
		this.entry = null;
	}
	
}
