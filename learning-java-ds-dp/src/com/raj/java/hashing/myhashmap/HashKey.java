package com.raj.java.hashing.myhashmap;

public interface HashKey {
	
	public int hashcode();
	public boolean equals();
}
