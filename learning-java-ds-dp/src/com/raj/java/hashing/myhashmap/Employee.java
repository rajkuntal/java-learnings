package com.raj.java.hashing.myhashmap;

public class Employee {
	
	public int id;
	public String name;
	
	public Employee(int id, String name){
		this.id = id;
		this.name = name;
	}
	
	public int hashCode(){
		return this.id%10;
	}
	
	public boolean equals(Employee e1){
		return (this.name.equalsIgnoreCase(e1.name));
	}

}
