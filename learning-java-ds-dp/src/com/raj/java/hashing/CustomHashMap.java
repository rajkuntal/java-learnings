package com.raj.java.hashing;

import java.util.ArrayList;

public class CustomHashMap {
	
	public ArrayList<ContainerObject> map;
	
	public CustomHashMap() {
		this.map = new ArrayList<ContainerObject>(10);
	}
	
	public boolean put(ContainerObject object, String value) {
		if(map.size() != 0) {
			ContainerObject alreadyHash = null;
			for(int i = 0; i < map.size(); i++) {
				ContainerObject inner = map.get(i);
				if(inner != null) {
					if(inner.hashCode() == object.hashCode()) {
						alreadyHash = inner;
						break;
					}
				}
				else {
					map.add(object);
				}
			}
			if(alreadyHash != null) {
				while(alreadyHash != null) {
					if(alreadyHash.next == null) {
						alreadyHash.next = object;
						break;
					} else
						alreadyHash = alreadyHash.next;
				}
			} else
				map.add(object);
		} else
			map.add(object);
		return true;
	}
	
	public ContainerObject get(ContainerObject object) {
		for(int i = 0; i < map.size(); i++) {
			ContainerObject inner = map.get(i);
			if(inner != null) {
				if(inner.hash == object.hashCode()) {
					while(inner != null) {
						if(inner.equals(object)) {
							return inner;
						} else
							inner = inner.next;
					}
				}
			}
		}
		return null;
	}
	

}
