package com.raj.java.hashing;

public class ContainerObject {
	public int id;
	public String name;
	public int hash;
	public ContainerObject next;
	
	public ContainerObject(int id, String name) {
		this.id = id;
		this.name = name;
		this.hash = this.hashCode();
		this.next = null;
	}
	
	public int hashCode() {
		if(id < 5)
			return 10;
		return 20;
	}
	
	public boolean equals(Object obj) {
		ContainerObject o = (ContainerObject) obj;
		if(this.name.equalsIgnoreCase(o.name))
			return true;
		return false;
	}
	
	public String toString(){
		return ("Id : "+id+", name : "+name+", hash : "+hash);
	}
}
