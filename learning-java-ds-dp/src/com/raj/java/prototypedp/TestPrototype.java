package com.raj.java.prototypedp;

public class TestPrototype {
	
	public static void main(String[] args) {

		HondaCar hondaCar1 = new HondaCar();
		
		HondaCar hondaCar2 = (HondaCar) hondaCar1.makeCar();
		
		System.out.println("hondaCar 1 - " + System.identityHashCode(hondaCar1));
		
		System.out.println("hondaCar 2 - " + System.identityHashCode(hondaCar2));
		
		
	}

}