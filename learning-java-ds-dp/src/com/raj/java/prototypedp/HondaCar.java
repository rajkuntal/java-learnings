package com.raj.java.prototypedp;

public class HondaCar implements Car {
	
	public HondaCar() {
		System.out.println("In Honda Car Factory");
	}
	
	

	@Override
	public Car makeCar() {
		System.out.println("Making Honda Car");
		Car car = null;
		try {
			car = (HondaCar)super.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return car;
	}

}
