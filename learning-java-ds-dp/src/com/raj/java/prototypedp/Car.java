package com.raj.java.prototypedp;

public interface Car extends Cloneable {
	
	public Car makeCar();

}
