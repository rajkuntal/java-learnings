package com.raj.java.staticinnerclass;

public class TestStaticClass {
	
	public static void main(String[] args) {
		OuterClass.InnerStaticClass innerStaticClass = new OuterClass.InnerStaticClass();
		innerStaticClass.displayInnerStaticClassMethod();
		
		OuterClass outerClass = new OuterClass();
		
		OuterClass.InnerClass innerClass = outerClass.new InnerClass(); 
		innerClass.displayInnerClassMethod();
		
	}

}
