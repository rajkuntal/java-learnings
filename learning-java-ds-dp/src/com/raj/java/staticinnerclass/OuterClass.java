package com.raj.java.staticinnerclass;

public class OuterClass {
	
	public String nonStaticVariable = "Outer class non static variable";
	public static String staticVariable = "Outer class static variable";
	
	public static class InnerStaticClass {
		
		public void displayInnerStaticClassMethod() {
			System.out.println("Inside static inner class -"+staticVariable);
		}
	}
	
	public class InnerClass {
		
		public void displayInnerClassMethod() {
			System.out.println("Inside non static inner class -"+staticVariable);
			System.out.println("Inside non static inner class -"+nonStaticVariable);
		}
	}

}
