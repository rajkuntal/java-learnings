package com.raj.java.polymorphism;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;

public class TestOverloading {
	
	public static void main(String[] args) {
		
		Car car = new Car();
		
		System.out.println("Car Lenght = "+ car.lenght +", Height = " +car.height+", Brand = "+car.brand +", Width = ");
		
		car.createCar(3, "Honda");
		System.out.println("Car Lenght = "+ car.lenght +", Height = " +car.height+", Brand = "+car.brand);
		
		car.createCar("Maruti", 2);
		System.out.println("Car Lenght = "+ car.lenght +", Height = " +car.height+", Brand = "+car.brand);
		
		car.createCar("Suzuki");
		System.out.println("Car Lenght = "+ car.lenght +", Height = " +car.height+", Brand = "+car.brand);
		
		HondaCar car1 = new HondaCar();
		
		System.out.println("Car Lenght = "+car1.width);
		
		car1.createCar(2);
		
		System.out.println("Car width = "+car1.width);
		
		// Exception Handling in method over riding
		
		try {
			car.readCarFile();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			car1.readCarFile();
		} catch (FileAlreadyExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
