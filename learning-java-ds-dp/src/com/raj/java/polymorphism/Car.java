package com.raj.java.polymorphism;

public class Car {
	
	public int lenght;
	public double height;
	public String brand;
	
	public Car() {
		
	}
	
	public void readCarFile() throws Exception{
		System.err.println("IO Exception occured in base class");
	}
	
	public void createCar(int length, String brand) {
		this.lenght = length;
		this.brand = brand;
	}
	
	// Method Overloading
	public void createCar(String brand, int length) {
		this.lenght = length;
		this.brand = brand;
	}
	
	// Method Overloading
	public void createCar(int length) {
		this.lenght = length;
	}
	
	
	// Method Overloading
	public void createCar(String brand) {
		this.brand = brand;
	}

}
