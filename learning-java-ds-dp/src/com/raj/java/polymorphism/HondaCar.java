package com.raj.java.polymorphism;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;

public class HondaCar extends Car{
	
	public String engineModel;
	public int width;
	
	
	// Method Overriding
	public void createCar(int width) {
		this.width = width;
	}
	
	public void readCarFile() throws IOException, FileAlreadyExistsException {
		System.err.println("IO Exception occured in child class");
	}

}
