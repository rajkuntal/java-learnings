package com.raj.java.serializedeserialize;

import java.io.Serializable;

public class Student implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public int rollNo;
	
	public String name;
	
	public static String lastName;
	
	public transient String middleName;
	
	public Student(int rollNo, String name, String lastName, String middleName) {
		this.rollNo = rollNo;
		this.name = name;
		this.lastName = lastName;
		this.middleName = middleName;
	}
	
	public String toString() {
		return ("Student - rollNo : "+ this.rollNo + " & name : " + this.name + " & middleName : " + this.middleName + " & lastName : " + this.lastName);
	}

}
