package com.raj.java.serializedeserialize;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class TestSerializeDeserialize {
	
	public static void main(String[] args) {
		Student student = new Student(101, "Raj", "Kuntal", "Kumar");
		Student std1;
		
		// Serialization
		try {
			OutputStream os = new FileOutputStream("ser.txt");
			ObjectOutput oo = new ObjectOutputStream(os);
			oo.writeObject(student);
			System.out.println("Serialized " + student.toString());
			os.close();
			oo.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//deserialization
		try {
			InputStream is = new FileInputStream("ser.txt");
			ObjectInput oi = new ObjectInputStream(is);
			std1 = (Student) oi.readObject();
			System.out.println("Desialized " + std1.toString());
			is.close();
			oi.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (ClassNotFoundException e) { 
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}

}
