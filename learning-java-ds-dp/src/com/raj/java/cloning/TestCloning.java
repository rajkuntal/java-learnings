package com.raj.java.cloning;

public class TestCloning {
	
	public static void main(String[] args) {
		
		Animal animal1 = new Animal(20);
		
		Animal animal2 = (Animal)animal1.clone();
		
		System.out.println("Animal 1 - " + System.identityHashCode(animal1));
		
		System.out.println("Animal 2 - " + System.identityHashCode(animal2));
		
		System.out.println("Animal 1 - Dog : " + System.identityHashCode(animal1.getDog()));
		
		System.out.println("Animal 2 - Dog :" + System.identityHashCode(animal2.getDog()));
		
		animal2.addWeight(10);
		animal2.increaseDogHeight(1);
		
		System.out.println("Animal 1 weight- " + animal1.weight);
		
		System.out.println("Animal 2 weight- " + animal2.weight);
		
		System.out.println("Animal 1 - Dog height- " + animal1.getDogHeight());
		
		System.out.println("Animal 2 - Dog height- " + animal2.getDogHeight());
	}
}
