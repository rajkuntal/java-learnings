package com.raj.java.cloning;

public class Animal implements Cloneable{
	
	public int weight;
	
	private Dog dog = new Dog(2);
	
	public Animal(int weight) {
		this.weight = weight;
	}
	
	public Animal clone() {
		
		Animal animal = null;
		try {
			animal = (Animal)super.clone();
			animal.dog = (Dog) dog.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return animal;
	}
	
	public void addWeight(int value) {
		this.weight = weight + value;
	}
	
	public void increaseDogHeight(int value) {
		dog.increaseHeight(value);
	}
	
	public int getDogHeight() {
		return dog.height;
	}
	
	public Dog getDog() {
		return this.dog;
	}
}
