package com.raj.java.overridding;

public class ParentProduct {
	
	public String parent;
	
	public void display() {
		System.out.println("this is parent class : "+this.getClass().getName());
	}
	
	public ParentProduct() {
		this.parent = "parent";
	}

}
