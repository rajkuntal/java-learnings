package com.raj.java.overridding;

public class Test {
	
	public static void main(String[] args) {
		ParentProduct pp = new ParentProduct();
		ParentProduct pp1  = new ChildProduct();
		
		pp.display();
		pp1.display();
	}

}
