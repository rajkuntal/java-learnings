package com.raj.java.overridding;

public class ChildProduct extends ParentProduct{
	
	public String child;
	
	public void display() {
		System.out.println("this is Child class : "+this.getClass().getName());
	}
	
	public ChildProduct() {
		super();
		this.child = "child";
	}

}
