package com.raj.java.generics;

public class GenericClass<T> {
	
	T obj;
	
	public GenericClass(T genericType) {
		this.obj = genericType;
	}
	
	public GenericClass() {
		
	}
	
	public T getGenericObject() {
		return obj;
	}
	
	public <T> void genericMethod(T generic) {
		System.out.println(generic.getClass().getName() + ":"+generic);
	}

}


