package com.raj.java.generics;

public class TestGenerics {
	
	public static void main(String[] args) {
		
		GenericClass<Integer> genreric1 = new GenericClass<Integer>(15);
		
		System.out.println(genreric1.getGenericObject());
		
		genreric1.genericMethod(10);
		
		GenericClass<String> stringGeneric = new GenericClass<String>("Raj Kumar");
		
		System.out.println(stringGeneric.getGenericObject());
		
		genreric1.genericMethod("Raj Kuntal");
		
		GenericClass generic = new GenericClass();
		
	}

}
