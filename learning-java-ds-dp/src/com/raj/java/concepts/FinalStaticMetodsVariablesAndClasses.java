package com.raj.java.concepts;

// FinalMetodsAndVariables
public class FinalStaticMetodsVariablesAndClasses {
	
	// final variables can't be reinitialized if once initialised
	final static int i;
	final int j;
	
	final static int k = 20;
	final int l = 30;
	
	public FinalStaticMetodsVariablesAndClasses() {
		j = 10;
	}
	
	// final static variables can be initialized inside of an static variable or at the time of declaration
	static {
		i = 10;
	}
	
	public static void staticDispay() {
	}
	
	// final method can be overloaded but can be over-ridden also
	public static void staticDispay(int x) {
	}
	
	public final void finalDispay() {
	}
	
	// final method can be overloaded but can't be over-ridden
	public final void finalDispay(int i) {
	}
	
	public static final void staticFinalDispay() {
	}

};


// final class can extends non-final class
final class FinalClass extends FinalStaticMetodsVariablesAndClasses {
	
	// static methods can be overridden
	public static void staticDispay() {
	}
	
};


//final class can't be extended
final class FinalClassCantBeExtended {
	
}


abstract class TestClass {
	
	public static void main(String[] args) {
		
		FinalStaticMetodsVariablesAndClasses obj1 = new FinalStaticMetodsVariablesAndClasses();
		
		System.out.println("Value of i - obj1 : "+ obj1.i);
		
		FinalStaticMetodsVariablesAndClasses obj2 = new FinalStaticMetodsVariablesAndClasses();
		//obj2.i = 10;
		System.out.println("Value of i - obj1 : "+ obj1.i);
		System.out.println("Value of i - obj2 : "+ obj2.i);
	}
};
