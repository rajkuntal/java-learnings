package com.raj.java.concepts;

class VolatileKeyword {
	
	int i = 10;
	
	public int counter = 1;
	
	public void updateVariable() {
		System.out.println("Thread - "+Thread.currentThread().getName() + "-Updating the variable");
		this.i = this.i + this.counter;
		counter++;
	}
	
	public int getVariable() {
		return this.i;
	}

};

class MyThread implements Runnable{
	
	public VolatileKeyword volatileKeyword;
	
	volatile int var = 1;
	
	public MyThread() {}
	
	public MyThread(VolatileKeyword volatileKeyword) {
		this.volatileKeyword = volatileKeyword;
	}
	
	public void run() {
		System.out.println("Accessing the Volatile keyword - current value of i = " + var + " - "+ Thread.currentThread().getName());
		try {
			if(Thread.currentThread().getName().equals("Thread-0"))
				Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Accessing the Volatile keyword - current value of i = " + var + " - "+ Thread.currentThread().getName());
		System.out.println("Thread is sleeping - " + Thread.currentThread().getName());
		var = var +1;
		try {
			if(Thread.currentThread().getName().equals("Thread-0"))
				Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("After change the Volatile keyword - current value of i = " + var + " - "+ Thread.currentThread().getName());
	}
	
};

class TestVolatile {
	
	public static void main(String[] args) {
		VolatileKeyword volatileKeyword = new VolatileKeyword();
		Thread myThread1 = new Thread(new MyThread());
		Thread myThread2 = new Thread(new MyThread());
		myThread1.start();
		myThread2.start();
	}
};
