package com.raj.java.immutable;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/*
1. Declare the class as final so it can’t be extended.
2. Make all fields private so that direct access is not allowed.
3. Don’t provide setter methods for variables
4. Make all mutable fields final so that it’s value can be assigned only once.
5. Initialize all the fields via a constructor performing deep copy.
6. Perform cloning of objects in the getter methods to return a copy rather than returning the actual object reference.

*/
public final class ImmutableStudent {
	
	private final int rollNo;
	
	private final String name;
	
	private final HashMap<String, Double> marksMapping;
	
	public ImmutableStudent(int rollNo, String name, HashMap<String, Double> marksMappping) {
		this.rollNo = rollNo;
		this.name = name;
		// Deep Copy
		this.marksMapping = new HashMap<>();
		Iterator<Map.Entry<String, Double>> it = marksMappping.entrySet().iterator();
		while(it.hasNext()) {
			Map.Entry<String, Double> entry = it.next();
			this.marksMapping.put(entry.getKey(), entry.getValue());
		}
		
		// Sollow Copy
		// this.marksMapping = marksMappping;
	}
	
	public int getRollNo() {
		return this.rollNo;
	}
	
	public String getName() {
		return this.name;
	}
	
	public Map<String, Double> getMarksMapping() {
		Map<String, Double> marksMapping = new HashMap<>();
		marksMapping = this.marksMapping;
		return marksMapping;
	}

}
