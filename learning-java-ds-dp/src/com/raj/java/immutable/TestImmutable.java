package com.raj.java.immutable;

import java.util.HashMap;

public class TestImmutable {
	
	public static void main(String[] args) {
		
		int rollNo = 101;
		String name = "Raj";
		HashMap<String, Double> marksMapping = new HashMap<String, Double>();
		marksMapping.put("Hindi", 70.22);
		marksMapping.put("English", 80.22);
		marksMapping.put("Science", 90.22);
		
		ImmutableStudent student1 = new ImmutableStudent(rollNo, name, marksMapping);
		
		System.out.println("Student1 - RollNo : "+student1.getRollNo());
		System.out.println("Student1 - Name : "+student1.getName());
		System.out.println("Student1 - RollNo : "+student1.getMarksMapping());
		
		marksMapping.put("Math", 92.22);
		
		rollNo = 105;
		name = "Sanju";
		ImmutableStudent student2 = new ImmutableStudent(rollNo, name, marksMapping);
		
		System.out.println("Student1 After Change in Params - RollNo : "+student1.getRollNo());
		System.out.println("Student1 After Change in Params - Name : "+student1.getName());
		System.out.println("Student1 After Change in Params - RollNo : "+student1.getMarksMapping());
		
		System.out.println("Student2 After Change in Params - RollNo : "+student2.getRollNo());
		System.out.println("Student2 After Change in Params - Name : "+student2.getName());
		System.out.println("Student2 After Change in Params - RollNo : "+student2.getMarksMapping());
		
		marksMapping.put("Social", 22.22);
		
		System.out.println("Local variable map ----------------------->"+marksMapping);
		
		System.out.println("Student1 After Change in local var - RollNo : "+student1.getRollNo());
		System.out.println("Student1 After Change in local var - Name : "+student1.getName());
		System.out.println("Student1 After Change in local var - RollNo : "+student1.getMarksMapping());
		
		System.out.println("Student2 After Change in local var - RollNo : "+student2.getRollNo());
		System.out.println("Student2 After Change in local var - Name : "+student2.getName());
		System.out.println("Student2 After Change in local var - RollNo : "+student2.getMarksMapping());
		
	}

}
