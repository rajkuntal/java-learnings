package com.raj.java.scanner;

import java.io.*;
import java.util.*;

public class ScannerTest {

	public static void main(String args[]) throws Exception {
		Scan scanner = new Scan();
		int n = scanner.nextInt();
		int input[] = new int[n];
		int input2[] = new int[n];
		for (int i = 0; i < n; i++) {
			input[i] = scanner.nextInt();
		}
		int result = 0;
		int curr = 0;
		for (int i = 0; i < n; i++) {
			int a = scanner.nextInt();
			boolean isFound = false;
			while (!isFound) {

				if (input[curr] == a) {
					input[curr] = -1;
					result++;
					curr++;
					isFound = true;
				} else if (input[curr] == -1) {
					curr++;
				} else {
					result++;
					curr++;
				}

				if (curr == n) {
					curr = 0;
				}
			}
		}
		System.out.println(result);
	}
}

class Scan {
	private byte[] buf = new byte[1024]; // Buffer of Bytes
	private int index;
	private InputStream in;
	private int total;

	public Scan() {
		in = System.in;
	}

	public int scan() throws IOException // Scan method used to scan buf
	{
		if (total < 0)
			throw new InputMismatchException();
		if (index >= total) {
			index = 0;
			total = in.read(buf);
			if (total <= 0)
				return -1;
		}
		return buf[index++];
	}

	public int nextInt() throws IOException {
		int integer = 0;
		int n = scan();
		while (isWhiteSpace(n)) // Removing starting whitespaces
			n = scan();
		int neg = 1;
		if (n == '-') // If Negative Sign encounters
		{
			neg = -1;
			n = scan();
		}
		while (!isWhiteSpace(n)) {
			if (n >= '0' && n <= '9') {
				integer *= 10;
				integer += n - '0';
				n = scan();
			} else
				throw new InputMismatchException();
		}
		return neg * integer;
	}

	private boolean isWhiteSpace(int n) {
		if (n == ' ' || n == '\n' || n == '\r' || n == '\t' || n == -1)
			return true;
		return false;
	}

}
