package com.raj.java.multithreading;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;

public class PubliserConsumerExample {
	
	private static Queue<Integer> list;
	
	public PubliserConsumerExample() {
		list = new LinkedList<>();
	}
	
	public synchronized static void addItem(Integer value){
		System.out.println("New value added by publiser : "+ value +"- Time :"+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
		list.add(value);
	}
	
	public synchronized Integer removeItem(){
		Integer value = null;
		if(list.isEmpty()){
			try {
				System.out.println("Consumer is waiting");
				this.wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		value = list.poll();
		System.out.println("value removed by consumer : "+ value +"- Time :"+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
		return value;
	}

}
