package com.raj.java.multithreading;

import java.util.Random;

public class TestPubSub {
	
	public static void main(String[] args) {
		PubliserConsumerExample object = new PubliserConsumerExample();
		Random r = new Random();
		Thread pub = new Thread(new Runnable() {
			@Override
			public void run() {
				for(int i = 0; i <= 10; i++){
					object.addItem(r.nextInt(10));
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
		
		Thread con = new Thread(new Runnable() {
			@Override
			public void run() {
				for (int i = 0; i <= 10; i++) {
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					object.removeItem();
				}
			}
		});
		
		pub.start();
		con.start();
	}

}
