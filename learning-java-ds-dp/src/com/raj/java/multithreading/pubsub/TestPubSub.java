package com.raj.java.multithreading.pubsub;

import java.util.LinkedList;
import java.util.Queue;

public class TestPubSub {
	
	public static void main(String[] args) {
		
		Queue<Integer> list = new LinkedList<>();
		
		Publiser pub = new Publiser(list);
		Consumer cun = new Consumer(list);
		
		Thread pub1 = new Thread(pub, "pub1");
		Thread pub2 = new Thread(pub, "pub2");
		
		Thread con1 = new Thread(cun, "con1");
		Thread con2 = new Thread(cun, "con2");
		
		pub1.start();
		con1.start();
		
		try {
			pub1.join();
			con1.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
