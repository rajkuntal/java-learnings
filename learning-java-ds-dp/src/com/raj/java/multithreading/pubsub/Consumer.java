package com.raj.java.multithreading.pubsub;

import java.util.Queue;

public class Consumer implements Runnable{

	private Queue<Integer> list;
	
	public Consumer(Queue<Integer> list) {
		this.list = list;
	}
	
	@Override
	public void run() {
		if(list.isEmpty()) {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		while(!list.isEmpty()) {
			System.out.println(Thread.currentThread().getName() + " " + list.poll());
		}
	}

}
