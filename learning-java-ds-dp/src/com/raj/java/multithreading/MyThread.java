package com.raj.java.multithreading;

public class MyThread implements Runnable {
	
	private int count = 0;
	
	@Override
	public void run() {
		processCounter();
	}
	
	public void processCounter() {
		String thread = Thread.currentThread().getName();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException ex) {
			
		}
		while(count < 10) {
			increaseCounter(thread);
		}
	}
	
	public void increaseCounter(String tName) {
		synchronized(this) {
			if(tName.equals("odd") && count%2 != 0) {
				try {
					this.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(tName.equals("even") && count%2 == 0) {
				try {
					this.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			count++;
			System.out.println(tName + " : " + count);
			this.notify();
		}
	}
}
