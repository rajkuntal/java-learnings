package com.raj.java.multithreading;

public class TestCounter {
	
	public static void main(String[] args) {
		
		MyThread th = new MyThread();
		Thread t1 = new Thread(th, "odd");
		Thread t2 = new Thread(th, "even");
		Thread t3 = new Thread(th, "odd");
		Thread t4 = new Thread(th, "even");
		
		t1.start();
		t2.start();
		t3.start();
		t4.start();
	}

}
