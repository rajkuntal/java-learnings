package com.raj.java.string;

public class TestString {
	
	public static void main(String[] args) {
		stringOperations();
	}
	
	private static void stringOperations() {
		String s1 = "new";
		String s2 = "new";
		
		String s5 = "nn";
		String s4 = new String("n");
		String s3 = (s4+"n").intern();
		
		if(s1==s2)
			System.out.println("s1 and s2 referce the same object");
		
		if(s1.equals(s3))
			System.out.println("s1 and s3 are same object");
		
		if(s1==s4)
			System.out.println("s1 and s4 referce the same object");
		
		if(s1.equals(s4))
			System.out.println("s1 and s4 are same object");
		
		if(s3==s5)
			System.out.println("s3 and s5 referce the same object");
		
		System.out.println(System.identityHashCode(s1) + "  " + System.identityHashCode(s2));
		
		String hello = "Hello", lo = "lo";
        System.out.print((hello == "Hello") + " ");
        System.out.print((hello == ("Hel"+"lo")) + " ");
        System.out.print((hello == ("Hel"+lo)) + " ");
        System.out.println(hello == ("Hel"+lo).intern());
	}

}
