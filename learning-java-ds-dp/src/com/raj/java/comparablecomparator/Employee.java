package com.raj.java.comparablecomparator;

import java.util.Comparator;

public class Employee implements Comparable<Employee>{
	
	public int id;
	public String name;
	public double salary;
	
	public Employee(int id, String name, double sal) {
		this.id=id;
		this.name=name;
		this.salary=sal;
	}
	
	public String toString(){
		return ("Id:"+id+", Name:"+name+", Salary:"+salary);
	}

	@Override
	public int compareTo(Employee o) {
		if(this.id<o.id)
			return -1;
		else if(this.id>o.id)
			return 1;
		else
			return 0;
	}
	
	public static Comparator<Employee> SalaryComparator = new Comparator<Employee>() {
		
		@Override
		public int compare(Employee o1, Employee o2) {
			return (int) (o1.salary-o2.salary);
		}
	};

}
