package com.raj.java.comparablecomparator;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

public class Test {
	
	public static void main(String[] args) {
		Employee[] emps = new Employee[5];
		emps[0] = new Employee(1, "Raj", 500.00);
		emps[1] = new Employee(4, "Ram", 200.00);
		emps[2] = new Employee(2, "Shyam", 50.00);
		emps[3] = new Employee(5, "Sanju", 100.00);
		emps[4] = new Employee(3, "Aman", 350.00);
		
		for(int i=0; i<emps.length;i++) {
			System.out.println(emps[i]);
		}
		// using Comparable
		Arrays.sort(emps);
		
		System.out.println("After Soring Employess using comparable");
		
		for(int i=0; i<emps.length;i++) {
			System.out.println(emps[i]);
		}
		
		// using Employee Id Comparator
		Arrays.sort(emps, new Comparator<Employee>() {

			@Override
			public int compare(Employee o1, Employee o2) {
				return (o1.id-o2.id);
			}
			
		});
		
		System.out.println("After Soring Employess using Id Comparator");
		
		for(int i=0; i<emps.length;i++) {
			System.out.println(emps[i]);
		}
		
		// using Salary Comparator
		Arrays.sort(emps, Employee.SalaryComparator);
		
		System.out.println("After Soring Employess using Salary Comparator");
		
		for(int i=0; i<emps.length;i++) {
			System.out.println(emps[i]);
		}
		
		// using Employee Name Comparator
		Arrays.sort(emps, new EmployeeNameComparator());
		
		System.out.println("After Soring Employess using Name Comparator");
		
		for(int i=0; i<emps.length;i++) {
			System.out.println(emps[i]);
		}
		
		
		
		// Using List
		
		List<Employee> empList = new LinkedList<>();
		for(int i=0; i<emps.length;i++) {
			empList.add(emps[i]);
		}
		System.out.println();
		System.out.println("Employees List before sorting");
		for(Employee emp : empList) {
			System.out.println(emp);
		}
		
		System.out.println();
		// Sorting using comparable
		Collections.sort(empList);
		System.out.println("Employees List After sorting using comparable(Id)");
		for(Employee emp : empList) {
			System.out.println(emp);
		}
		
		System.out.println();
		// Sorting using comparable
		Collections.sort(empList, new EmployeeNameComparator());
		System.out.println("Employees List After sorting using EmployeeNameComparator");
		for(Employee emp : empList) {
			System.out.println(emp);
		}
		
		// Using Map
		System.out.println();
		System.out.println("Using tree map");
		
		SortedMap<Employee, String> map = new TreeMap(Employee.SalaryComparator);
		map.put(new Employee(1, "Sanju", 200),"Sanju");
		map.put(new Employee(3, "Raj", 50),"Raj");
		map.put(new Employee(2, "Aman", 150), "Aman");
		
		Set<Entry<Employee, String>> entries = map.entrySet();
		Iterator<Entry<Employee, String>> it = entries.iterator();
		while(it.hasNext()){
			Entry<Employee, String> e = it.next();
			System.out.println("Key :"+e.getKey()+", value : "+e.getValue());
		}
		
	}

}
