package com.raj.java.inputs;

import java.io.IOException;
import java.util.Scanner;

public class ConsoleInputs {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Count array input");
		int sum = sc.nextInt();
		int[] a = new int[sum];
		System.out.println("Int array input");
		for(int i=0; i<sum; i++) {
			a[i] = sc.nextInt();
		}
		
		System.out.println("Char array input");
		char[] c = new char[sum];
		for(int i=0; i<sum; i++) {
			try {
				c[i] = (char)System.in.read();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		System.out.println("Print Int Array");
		for(int i=0; i<sum; i++) {
			System.out.println(a[i]+",");
		}
		
		System.out.println("Print Char Array");
		for(int i=0; i<sum; i++) {
			System.out.println(c[i]+",");
		}
	}

}
