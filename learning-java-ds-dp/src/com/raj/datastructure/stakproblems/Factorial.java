package com.raj.datastructure.stakproblems;

public class Factorial {
	
	public static int factorial(int n){
		if(n>2)
			return n*factorial(n-1);
		else
			return 1;
	}
	
	public static void main(String[] args) {
		int n = 10;
		System.out.println("factorial of : "+n+"-->"+factorial(n));
		System.out.println("Multiply 4 and 3 : "+multiplication(4, 3));
	}
	
	public static int multiplication(int a, int b) {
		if(b==0)
			return 0;
		if(b%2==0) {
			int r = multiplication(a+a, b/2);
			return r;
		}
		int rr = multiplication(a+a, b/2)+a;
		return rr;
	}

}
