package com.raj.datastructure.stakproblems;

public class StakProblems {
	
	public static int[] findNxtGrtToNxtSmlr(int[] a) {
		
		int[] gt = findNextGreater(a, a.length);
		int[] sl = new int[a.length];
		for(int i= 0; i<a.length; i++){
			for(int j=gt[i]+1; j<a.length; j++) {
				if(a[gt[i]] == -1) {
					sl[i] = -1;
					break;
				}
				else if(a[j] < a[gt[i]]) {
					sl[i] = a[gt[j]];
					break;
				} else
					sl[i] = -1;
			}
		}
		return sl;
		
	}
	
	public static int[] findNextGreater(int[]a, int n) {
		int[] gt = new int[n];
		for(int i=0; i<n; i++) {
			if(i==n-1) {
				gt[i] = -1;
			}
			for(int j=i+1;j<n;j++) {
				if(a[j]>a[i]) {
					gt[i] = j;
					break;
				} else
					gt[i] = -1;
			}
		}
		return gt;
	}
	
	public static void main(String[] args) {
		int[] a = {4,8,2,1,9,5,6,3};
		int[] sl = findNxtGrtToNxtSmlr(a);
		for(int i=0; i<a.length;i++) {
			System.out.println(a[i]+"-->"+sl[i]);
		}
	}
	
}
