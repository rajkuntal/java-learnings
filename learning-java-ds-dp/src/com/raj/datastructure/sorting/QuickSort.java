package com.raj.datastructure.sorting;

import java.util.Arrays;

public class QuickSort {
	
	static int[] arr = {8,78,45,98,3,209,6,67,3,65,4,23,40};
	
	public static int partition(int start, int end) {
		int pivotIndex = (start+end)/2;
		int pivot = arr[pivotIndex];
		while(start <= end ) {
			while(arr[start] < pivot) {
				start++;
			}
			while(arr[end] > pivot) {
				end--;
			}
			if(start <= end ) {
				System.out.println("Before Swaping of " + arr[start] + "-"+start+ "&" + arr[end] + "-" + end +" : " +Arrays.toString(arr));
				swap(start, end);
				System.out.println("After Swaping of " + arr[start] + "-"+start+ "&" + arr[end] + "-" + end +" : " +Arrays.toString(arr));
				start++;
				end--;
			}
		}
		return start;
	}
	
	public static void swap(int i, int j) {
		int tmp = arr[j];
		arr[j] = arr[i];
		arr[i] = tmp;
	}
	
	public static void quickSort(int start, int end) {
		for(int i= start; i <= end; i++) {
			System.out.print(arr[i] + ",");;
		}
		System.out.println("-----------------");
		int id = partition(start, end);
		for(int i= start; i <= end; i++) {
			System.out.print(arr[i] + ",");;
		}
		System.out.println();
		if(start < id-1)
			quickSort(start, id-1);
		if(end > id)
			quickSort(id, end);
		
	}
	
	public static void main(String[] args) {
		
		quickSort(0, arr.length-1);
		System.out.println(Arrays.toString(arr));
		
	}
	
}
