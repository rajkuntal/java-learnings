package com.raj.datastructure.string;

public class PrintCombinationANG {
	
	public static void main(String[] args) {
		String s = "ABC";
		char[] arr = s.toCharArray();
		printComb(arr, 0);
	}
	
	public static void printComb(char[] arr, int shift){
		while(shift < arr.length - 2){
			char[] tmp = shiftByOne(arr.clone(), shift, arr.length-1);
			printCombination(tmp, shift, 0);
			shift++;
		}
	}
	
	public static void printCombination(char[] arr, int fix, int start) {
		printHere(arr);
		if (fix > 0 && start > 0 && fix == start)
			return;
		if (fix < arr.length - 1 && start < arr.length - 1) {
			char[] tmp = shiftByOne(arr.clone(), start, arr.length - 1);
			printCombination(tmp, fix + 1, start);
			printCombination(tmp, fix, start + 1);
		}
	}
	
	
	public static char[] shiftByOne(char[] arr, int start, int end){
		if(start > 0){
			for(int i = start+1; i < end+1; i++){
				char tmp = arr[i-1];
				arr[i-1] = arr[i];
				arr[i] = tmp;
			}
		}
		return arr;
	}
	
	public static void printHere(char[] arr){
		for(int k = 0; k < arr.length; k++){
			System.out.print(arr[k]);
		}
		System.out.println();
	}

}
