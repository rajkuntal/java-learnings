package com.raj.datastructure.string;

public class ReverseWordsInString {
	
	public static void main(String[] args) {
		String s = "i.am.going.to.patna";
		System.out.println(reverseWords(s));
	}
	
	private static String reverseWords(String s) {
		s= s.replace(".", " ");
		String[] arr = s.split(" ");
		int first = 0;
		int last = arr.length - 1;
		while((first != last) || (first < last)) {
			String tmp = arr[first];
			arr[first] = arr[last];
			arr[last] = tmp;
			first++;
			last--;
		}
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<arr.length; i++) {
			sb.append(arr[i]+".");
		}
		return sb.toString();
	}

}
