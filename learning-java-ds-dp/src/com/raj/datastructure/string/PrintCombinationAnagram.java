package com.raj.datastructure.string;

public class PrintCombinationAnagram {
	
//	public static void main(String[] args) {
//		String s = "ABCD";
//		char[] arr = s.toCharArray();
//		char[] tmp = new char[arr.length];
//		int len = arr.length;
//		for(int i = 0; i < len; i++){
//			if(i>0)
//				arr = shiftByOne(arr, 1, len);
//			printCombination(arr);
//			char[] tmp2 = arr.clone();
//			for(int j = 2; j < len; j++){
//				tmp = shiftByOne(tmp2, j, len);
//				printCombination(tmp);
//			}
//			System.out.println();
//		}
//	}
	
	public static void main(String[] args) {
		String s = "ABCD";
		char[] arr = s.toCharArray();
		int len = arr.length;
		for(int i = 0; i < len; i++){
			if(i>0)
				arr = shiftByOne(arr, 1, len);
			printHere(arr);
			for(int j = 2; j < len; j++){
				printCombination(arr.clone(), j);
			}
			System.out.println();
		}
	}
	
	public static void printCombination(char[] arr, int start){
		int len = arr.length;
		char[] tmp = arr;
		for(int i = start; i < len; i++){
			for(int j = 2; j < len; j++){
				tmp = shiftByOne(tmp.clone(), j, len);
				printHere(tmp);
			}
		}
	}
	
	public static char[] shiftByOne(char[] arr, int start, int end){
		if(start > 0){
			for(int i = start; i < end; i++){
				char tmp = arr[i-1];
				arr[i-1] = arr[i];
				arr[i] = tmp;
			}
		}
		return arr;
	}
	
	public static void printHere(char[] arr){
		for(int k = 0; k < arr.length; k++){
			System.out.print(arr[k]);
		}
		System.out.println();
	}

}
