package com.raj.datastructure.matrix;

public class MatrixConversion {
	
	public static void main(String[] args) {
		
		int[][] matrix = {	{1,2,3},
						  	{4,5,6},
						  	{7,8,9},
						  };
		
		for(int i = 0; i < 3; i++){
			if(i%2 == 0){
				matrix = evenReplace(matrix, i, 3);
			} else{
				matrix = oddReplace(matrix, i, 3);
			}
		}
		
		printMatrix(matrix, 3, 3);
		
	}
	
	public static int[][] evenReplace(int[][] matrix, int i, int n){
		for(int j = 0; j < n; j++){
			swap(matrix, i, j);
		}
		return matrix;
	}
	
	public static int[][] oddReplace(int[][] matrix, int i, int n){
		for(int j = n-1; j >= 0; j--){
			swap(matrix, i, j);
		}
		return matrix;
	}
	
	public static int[][] swap(int[][] matrix, int i, int j){
		int tmp = matrix[i][j];
		matrix[i][j] = matrix[j][i];
		matrix[j][i] = tmp;
		return matrix;
	}
	
	public static void printMatrix(int[][] matrix, int m, int n){
		for(int i = 0; i < m; i++){
			for(int j = 0; j < n; j++){
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
	}

}
