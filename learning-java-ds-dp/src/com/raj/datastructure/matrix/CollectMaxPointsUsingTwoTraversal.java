package com.raj.datastructure.matrix;

import java.util.*;

public class CollectMaxPointsUsingTwoTraversal {

	public static void main(String[] args) throws java.lang.Exception {
		Functionality f = new Functionality();
		f.takeInput();
	}
}

class Functionality {
	int[][][] dp = new int[5][4][4];
	int R = 5, C = 4;

	void takeInput() {
		int[][] mat = new int[][] { { 3, 6, 8, 2 }, { 5, 2, 4, 3 }, { 1, 1, 20, 10 }, { 1, 1, 20, 10 },
				{ 1, 1, 20, 10 }, };

		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 4; j++) {
				Arrays.fill(dp[i][j], -1);
			}
		}
		int ans = findMaxSum(mat, 0, 0, 3);
		System.out.println(ans);
	}

	int findMaxSum(int[][] mat, int x, int y1, int y2) {
		if (!isValid(x, y1, y2)) {
			return Integer.MIN_VALUE;
		}

		if (x == R - 1) {
			if (y1 == 0 && y2 == C - 1) {
				return mat[x][y1] + mat[x][y2];
			} else {
				return Integer.MIN_VALUE;
			}
		}

		if (dp[x][y1][y2] != -1) {
			return dp[x][y1][y2];
		}

		int ans = Integer.MIN_VALUE;

		ans = Math.max(ans, findMaxSum(mat, x + 1, y1 - 1, y2));
		ans = Math.max(ans, findMaxSum(mat, x + 1, y1, y2));
		ans = Math.max(ans, findMaxSum(mat, x + 1, y1 + 1, y2));
		
		ans = Math.max(ans, findMaxSum(mat, x + 1, y1 - 1, y2 - 1));
		ans = Math.max(ans, findMaxSum(mat, x + 1, y1, y2 - 1));
		ans = Math.max(ans, findMaxSum(mat, x + 1, y1 + 1, y2 - 1));
		
		ans = Math.max(ans, findMaxSum(mat, x + 1, y1 - 1, y2 + 1));
		ans = Math.max(ans, findMaxSum(mat, x + 1, y1, y2 + 1));
		ans = Math.max(ans, findMaxSum(mat, x + 1, y1 + 1, y2 + 1));

		ans = ans + mat[x][y1] + mat[x][y2];

		dp[x][y1][y2] = ans;

		return ans;

	}

	Boolean isValid(int x, int y1, int y2) {
		if (x >= 0 && x < R && y1 >= 0 && y1 < C && y2 >= 0 && y2 < C && y1 != y2) {
			return true;
		}
		return false;
	}

}
