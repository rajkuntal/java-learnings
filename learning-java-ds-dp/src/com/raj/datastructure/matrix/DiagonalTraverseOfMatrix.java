package com.raj.datastructure.matrix;

public class DiagonalTraverseOfMatrix {
	
	public static void main(String[] args) {
		
		int[][] matrix = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16},{17,18,19,20}};
		printMatrix(matrix, 5, 4);
	}
	
	public static void printMatrix(int[][] matrix, int m, int n){
		for(int i = 0; i < m; i++){
			if(i < m-1){
				printDiagonally(matrix, i, 0, m, n);
			} else {
				for(int j = 0; j < n; j++){
					printDiagonally(matrix, i, j, m, n);
				}
			}
		}
	}
	
	public static void printDiagonally(int[][] matrix, int i, int j, int m, int n){
		while(i >= 0 && i < m && j >= 0 && j < n){
			System.out.print(matrix[i][j] + " ");
			i--;
			j++;
		}
		System.out.println();
	}

}
