package com.raj.datastructure.matrix;

public class MatrixProblems {
	
	
	private static int findSum(int [][] matrix, int i, int j, int k, int l){
		int sum = 0;
		if (i<k && j<l) {
			sum = matrix[i][j]+Math.min(findSum(matrix, i, j+1, k, l), findSum(matrix, i+1, j, k, l));
		}else if (i == k && j == l){
			sum = matrix[i][j];
		}
		else if (i == k){
			sum = matrix[i][j]+findSum(matrix, i, j+1, k, l); 
		} else if (j == l){
			sum = matrix[i][j]+findSum(matrix, i+1, j, k, l); 
		}		
		return sum;
		
	}
	
	public static void main(String[] args) {
		
		int[][] mt = {{1,2,3},{4,8,2},{1,5,3}};
		
		//System.out.println(findSum(mt, 0, 0, 2, 2));
		System.out.println();
		
		int[][]mt1 = {
						{4,2,3,4},
						{2,9,1,10},
						{15,1,3,0},
						{16,92,41,44}
					 };
		System.out.println(find(mt1,0,0,3,3));
	}
	
	private static int find(int[][] mt, int i, int j, int k, int l) {
		int sum = 0;
		 if(i<k && j>0 && j<l) {
			 sum = mt[i][j] + Math.max(find(mt, i+1, j, k, l), Math.max(find(mt, i+1, j-1, k, l), find(mt, i+1, j+1, k, l)));
		 } else if(i<k && j==0) {
			 sum = mt[i][j] + Math.max(find(mt, i+1, j, k, l), find(mt, i+1, j+1, k, l));
		 } else if(i<k && j==l) {
			 sum = mt[i][j] + Math.max(find(mt, i+1, j-1, k, l), find(mt, i+1, j, k, l));
		 } else if(i==k) {
			 sum = mt[i][j];
		 }
		 System.out.print(" ");
		 return sum;
	}
	

}
