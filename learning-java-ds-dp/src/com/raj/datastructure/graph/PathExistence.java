package com.raj.datastructure.graph;

import java.util.LinkedList;
import java.util.Queue;

/* Existence of Path from source to destination
		A value of cell 1 means Source.
		A value of cell 2 means Destination.
		A value of cell 3 means Blank cell.
		A value of cell 0 means Blank Wall. 
*/

public class PathExistence {
	
	public static void main (String[] args) {
		int[][] m = {
					 {0,3,2},
					 {3,3,0},
					 {1,3,0}
				    };
		int i = isPathExists(m, 3);
		System.out.println();
		System.out.println(i);
		
		int[][] n = {
					 {0,3,1,0},
					 {3,0,3,3},
					 {2,3,0,3},
					 {0,3,3,3}
			    	};
		
		int j = isPathExists(n, 4);
		System.out.println();
		System.out.println(j);
		
	}
	
	public static int isPathExists(int[][] m, int n) {
	    int k = 0;
	    int l = 0;
	    boolean source = false;
	    int pathExists = 0;
	    for(int i = 0; i<n; i++) {
	        for(int j = 0; j<n; j++){
	            if(m[i][j] == 1) {
	                source = true;
	                k = i;
	                l = j;
	                break;
	            }
	            if(source)
	                break;
	        }
	    }
	    Queue<GraphNode> q = new LinkedList<GraphNode>();
	    boolean[][] v = new boolean[n][n];
	    q.add(new GraphNode(m[k][l], k, l));
	    v[k][l] = true;
	    while(!q.isEmpty()) {
	        GraphNode node = q.poll();
	        System.out.print(node.value + "-->");
	        if(node.value == 2) {
	            pathExists = 1;
	            break;
	        }
	        // up
	        k = node.k-1;
	        l= node.l;
            if(isSafe(n,k,l) && !v[k][l] && m[k][l] != 0){
                q.add(new GraphNode(m[k][l], k, l));
                v[k][l] = true;
            }
            //down
            k = node.k+1;
	        l= node.l;
	        if(isSafe(n,k,l) && !v[k][l] && m[k][l] != 0){
                q.add(new GraphNode(m[k][l], k, l));
                v[k][l] = true;
            }
	        //left
            k = node.k;
	        l= node.l-1;
	        if(isSafe(n,k,l) && !v[k][l] && m[k][l] != 0){
                q.add(new GraphNode(m[k][l], k, l));
                v[k][l] = true;
            }
	        //right
            k = node.k;
	        l= node.l+1;
	        if(isSafe(n,k,l) && !v[k][l] && m[k][l] != 0){
                q.add(new GraphNode(m[k][l], k, l));
                v[k][l] = true;
            }
	   }
	   return pathExists;
	}
	
	public static boolean isSafe(int n, int k, int l){
	    if(k>=0 && k<n && l>=0 && l<n) {
	        return true;
	    }
	    return false;
	}
	
	static class GraphNode {
	    int value;
	    int k;
	    int l;
	    public GraphNode(int v, int k, int l) {
	        this.value = v;
	        this.k = k;
	        this.l = l;
	    }
	}

}
