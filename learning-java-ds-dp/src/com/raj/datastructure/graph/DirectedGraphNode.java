package com.raj.datastructure.graph;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

public class DirectedGraphNode {
	
	private int value;
	private List<DirectedGraphNode> adjacentNodes;
	
	public DirectedGraphNode(int v) {
		this.value = v;
		this.adjacentNodes = new LinkedList<>();
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public List<DirectedGraphNode> getAdjacentNodes() {
		return adjacentNodes;
	}
	public void setAdjacentNodes(List<DirectedGraphNode> adjacentNodes) {
		this.adjacentNodes = adjacentNodes;
	}
	
	public void addAdjacent(DirectedGraphNode aajacent) {
		this.adjacentNodes.add(aajacent);
	}
	
	public String BFS() {
		HashMap<DirectedGraphNode, Boolean> visited = new HashMap<>();
		Queue<DirectedGraphNode> q = new LinkedList<>();
		StringBuilder sb = new StringBuilder();
		if(this != null) {
			q.add(this);
			visited.put(this, true);
			while(!q.isEmpty()) {
				DirectedGraphNode node = q.poll();
				sb.append(node.getValue());
				sb.append(" ");
				if(node.getAdjacentNodes() != null) {
					for(DirectedGraphNode n : node.getAdjacentNodes()) {
						if(!visited.containsKey(n)) {
							q.add(n);
							visited.put(n,true);
						}
					}
				}
			}
		}
		return sb.toString();
	}
	
	public String DFS(DirectedGraphNode node, Set<DirectedGraphNode> set, StringBuilder sb) {
		if(node != null) {
			set.add(node);
			sb.append(node.getValue());
			sb.append(" ");
			int i = 0;
			while(i < node.getAdjacentNodes().size()) {
				DirectedGraphNode node1 = node.getAdjacentNodes().get(i);
				if(!set.contains(node1)) {
					DFS(node1, set, sb);
				}
				i++;
			}
		}
	return sb.toString();
	}

}
