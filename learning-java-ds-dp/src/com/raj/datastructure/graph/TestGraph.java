package com.raj.datastructure.graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class TestGraph {
	
	public static void main(String[] args) {
		DirectedGraphNode g1 = new DirectedGraphNode(2);
		
		DirectedGraphNode g2 = new DirectedGraphNode(0);
		
		DirectedGraphNode g3 = new DirectedGraphNode(1);
		
		DirectedGraphNode g4 = new DirectedGraphNode(3);
		
		g1.addAdjacent(g2);
		g1.addAdjacent(g4);
		g2.addAdjacent(g1);
		g2.addAdjacent(g3);
		g3.addAdjacent(g1);
		g4.addAdjacent(g4);
		
		System.out.println("g1 BFS : " + g1.BFS());
		System.out.println("g2 BFS : " + g2.BFS());
		System.out.println("g3 BFS : " + g3.BFS());
		System.out.println("g4 BFS : " + g4.BFS());
		
		System.out.println("g1 DFS : "+g1.DFS(g1, new HashSet<>(), new StringBuilder()));
		System.out.println("g2 DFS : "+g2.DFS(g2, new HashSet<>(), new StringBuilder()));
		
	}

}
