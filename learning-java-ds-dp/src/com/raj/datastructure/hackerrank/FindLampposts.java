package com.raj.datastructure.hackerrank;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class FindLampposts {
	
	public static void main(String[] args) {
		Set<String> set = new HashSet<>();
		Scanner sc = new Scanner(System.in);
		try {
			givenUsingPlainJava_whenConvertingAnInputStreamToAByteArray_thenCorrect();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long n = sc.nextInt();
		//System.out.println("n-"+n);
		long m = sc.nextInt();
		//System.out.println("m-"+m);
		int k = sc.nextInt();
		//System.out.println("k-"+k);
		for(int i=0; i < k; i++) {
			long r = sc.nextLong();
			//System.out.println("r-"+r);
			long c1 = sc.nextLong();
			//System.out.println("c1-"+c1);
			long c2 = sc.nextLong();
			//System.out.println("c2-"+c2);
			
			for(;c1 <= c2; c1++){
				set.add(String.valueOf(r)+String.valueOf(c1));
			}
               
		}
		long total = n*m-set.size();
		System.out.println(total);
    }
	
	public static boolean diffOne(long i, long j){
		if(i==j || (i-j == 1) || (i-j == -1))
			return true;
		return false;
	}
	
    public static long countLampPosts(long n, long m, Map<Long,List<TrainCell>> map){
    	long numberOfLampPosts = 0;
        for(long i=1; i <= n; i++){
            if(!map.containsKey(i)){
            	numberOfLampPosts = numberOfLampPosts + m;
            } else {
            	numberOfLampPosts = numberOfLampPosts + countLampPostsInRow(i, m, map);
            }
        }
        return numberOfLampPosts;
    }
    
    public static long countLampPostsInRow(long n, long m, Map<Long,List<TrainCell>> map){
    	List<TrainCell> list = map.get(n);
    	long total = 0;
    	for( TrainCell cell : list){
    		total = ((cell.c2-cell.c1) + 1) + total;
    	}
    	total = m - total;
    	return total;
    }
    
	public static void givenUsingPlainJava_whenConvertingAnInputStreamToAByteArray_thenCorrect() throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		StringBuilder everything = new StringBuilder();
	    String line;
	    while( (line = reader.readLine()) != null) {
	       everything.append(line);
	    }
	    System.out.println(everything.toString());
	};
    
    static class TrainCell implements Comparable<TrainCell>{
        public long c1;
        public long c2;
        public TrainCell next;
        public TrainCell(long c1, long c2){
            this.c1 = c1;
            this.c2 = c2;
            this.next = null;
        }
        public String toString(){
            return ("c1:"+c1+"-c2:"+c2);
        }
		@Override
		public int compareTo(TrainCell o) {
			if(this.c2<o.c2)
				return -1;
			else if(this.c2>o.c2)
				return 1;
			else
				return 0;
		}
    };

}
