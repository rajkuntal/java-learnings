package com.raj.datastructure.hackerrank;

import java.util.Map;
import java.util.Scanner;

/**
 * 
 * Christy is interning at HackerRank. One day she has to distribute some
 * chocolates to her colleagues. She is biased towards her friends and may have
 * distributed the chocolates unequally. One of the program managers gets to
 * know this and orders Christy to make sure everyone gets equal number of
 * chocolates.
 * 
 * But to make things difficult for the intern, she is ordered to equalize the
 * number of chocolates for every colleague in the following manner,
 * 
 * For every operation, she can choose one of her colleagues and can do one of
 * the three things.
 * 
 * She can give one chocolate to every colleague other than chosen one. She can
 * give two chocolates to every colleague other than chosen one. She can give
 * five chocolates to every colleague other than chosen one. Calculate minimum
 * number of such operations needed to ensure that every colleague has the same
 * number of chocolates.
 * 
 * Input Format
 * 
 * First line contains an integer denoting the number of testcases. testcases
 * follow. Each testcase has lines. First line of each testcase contains an
 * integer denoting the number of colleagues. Second line contains N space
 * separated integers denoting the current number of chocolates each colleague
 * has.
 * 
 * Constraints
 * 
 * 
 * 
 * Number of initial chocolates each colleague has <
 * 
 * Output Format
 * 
 * lines, each containing the minimum number of operations needed to make sure
 * all colleagues have the same number of chocolates.
 * 
 * Sample Input
 * 
 * 1 4 2 2 3 7 Sample Output
 * 
 * 2 Explanation
 * 
 * 1st operation: Christy increases all elements by 1 except 3rd one 2 2 3 7 ->
 * 3 3 3 8 2nd operation: Christy increases all element by 5 except last one 3 3
 * 3 8 -> 8 8 8 8
 * 1 51 512 125 928 381 890 90 512 789 469 473 908 990 195 763 102 643 458 366 684 857 126 534 974 875 459 892 686 373 127 297 576 991 774 856 372 664 946 237 806 767 62 714 758 258 477 860 253 287 579 289 496
 *
 */

public class EqualElements {

	public static void main(String[] args) {
//		Scanner sc = new Scanner(System.in);
//		int numberOfTests = sc.nextInt();
//		for(int i = 1; i <= numberOfTests; i++){
//			int noOfElememts = sc.nextInt();
//			int[] arr = new int[noOfElememts];
//			for(int j = 0; j < arr.length; j++){
//				int t = sc.nextInt();
//				arr[j] = t;
//			}
//			System.out.println(findMinOperation(arr));
//		}
		System.out.println(getElementCount(100, 62, 0, 0 ,0, Integer.MAX_VALUE));
	}

	public static int findMinOperation(int[] arr) {
		int minimum = Integer.MAX_VALUE;
		for(int i = 0; i < arr.length; i++){
			if(arr[i] < minimum)
				minimum = arr[i];
		}
		int operations = Integer.MAX_VALUE;
		for(;minimum >= 0; minimum--){
			int[] offset = new int[arr.length];
			int ops = 0;
			for(int i = 0; i < arr.length; i++){
				offset[i] = arr[i] - minimum;
				ops = ops + getElementCount(offset[i], minimum, 0, 0 ,0, Integer.MAX_VALUE);
			}
			if(ops < operations)
				operations = ops;
		}
		return operations;
	}
	
	public static int getElementCount(int number, int minimum, int first, int second, int third, int count, Map<Integer, Integer> map){
		if(number < minimum){
			return count;
		}
		int tmp = count;
		tmp = first+second+third;
		if(map.containsKey(number)){
			map.put(key, value)
		}
		if (number == minimum) {
			if(tmp < count){
				return tmp;
			}
		}
		count = getElementCount(number - 5, minimum, first, second, third+1, count, map);
		count = getElementCount(number - 2, minimum, first, second+1, third, count, map);
		count = getElementCount(number - 1, minimum, first+1, second, third, count, map);
		return count;
	}
}
