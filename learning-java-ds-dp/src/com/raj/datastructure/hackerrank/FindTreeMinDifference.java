package com.raj.datastructure.hackerrank;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FindTreeMinDifference {

	/*
	 * Anna loves graph theory! She has an -vertex tree, , where each vertex :
	 * 
	 * Is indexed with a unique integer from to . Contains a data value, . Anna
	 * observes that cutting any edge, , in results in the formation of two
	 * separate trees denoted by and . She also defines the following:
	 * 
	 * The sum of a tree is the sum of the values for all vertices in the tree.
	 * The difference between two trees created by cutting edge is denoted by .
	 * Given the definition of tree , remove some edge such that the value of is
	 * minimal. Then print the value of the minimum possible as your answer.
	 * 
	 * Note: The tree is always rooted at vertex .
	 * 
	 * Input Format
	 * 
	 * The first line contains an integer, , denoting the number of vertices in
	 * the tree. The second line contains space-separated integers where each
	 * integer denotes the value of . Each of the subsequent lines contains two
	 * space-separated integers, and , describing edge in tree .
	 * 
	 * Constraints
	 * 
	 * 
	 * , where . Output Format
	 * 
	 * A single line containing the minimum possible for tree .
	 * 
	 * Sample Input
	 * 
	 * 6 100 200 100 500 100 600 1 2 2 3 2 5 4 5 5 6 
	 * 
	 * Sample Output
	 * 
	 * 400
	 */
	
	public static void main(String[] args) {
//		TreeNode root = new TreeNode(100);
//		TreeNode node1 = new TreeNode(200);
//		root.childs.add(node1);
//		TreeNode node2 = new TreeNode(100);
//		TreeNode node3 = new TreeNode(100);
//		node1.childs.add(node2);
//		node1.childs.add(node3);
//		node2.childs.add(new TreeNode(500));
//		node2.childs.add(new TreeNode(600));
//		TreeNode diff = new TreeNode(1600, null);
//		updateSum(root, diff);
//		System.out.println(diff.data);
		
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		TreeNode[] nodes = new TreeNode[n];
		long total = 0 ;
		for(int i=0; i < n; i++) {
			long data = sc.nextLong();
			nodes[i] = new TreeNode(data);
			nodes[i].vertexNumber = i+1;
			total = total + data;
		}
		for(int i=0; i < n-1; i++) {
			int n1 = sc.nextInt();
			int n2 = sc.nextInt();
			TreeNode node = nodes[n1-1];
			TreeNode node1 = nodes[n2-1];
			node.childs.add(nodes[n2-1]);
			node1.childs.add(nodes[n1-1]);
		}
		TreeNode diff = new TreeNode(total, null);
		updateSum(nodes[0], diff, total);
		System.out.println(diff.data);
		
//		int n = 0;
//		long total = 0 ;
		Scanner inputReader = new Scanner(System.in);
		StringBuilder sb = new StringBuilder();
		String in = "";
		sb.append(inputReader.next());
		inputReader.close();
		System.out.println(sb.toString());
//		while(in != null){
//			sb.append(in);
//		}
//		String input = in.toString();
//		String[] array = input.split(" ");
//		n = Integer.parseInt(array[0]);
//		TreeNode[] nodes = new TreeNode[n];
//		for(int i=1, j = 0; i <= n; i++, j++) {
//			long data = Integer.parseInt(array[i]);
//			nodes[j] = new TreeNode(data);
//			total = total + data;
//		}
//		for(int i=n+1; i < array.length-1;) {
//			int n1 = Integer.parseInt(array[i]);
//			int n2 = Integer.parseInt(array[i+1]);
//			TreeNode node = nodes[n1-1];
//			TreeNode node1 = nodes[n1-2];
//			node.childs.add(nodes[n2-1]);
//			node1.childs.add(nodes[n1-1]);
//			i = i+2;
//		}
//		TreeNode diff = new TreeNode(total, null);
//		updateSum(nodes[5], diff, total);
//		System.out.println(diff.data);
	}
	
	public static long updateSum(TreeNode node, TreeNode difference, long total){
		if(node == null)
			return 0;
		long sum = node.data;
		node.visited = true;
		for(TreeNode child : node.childs){
			if(!child.visited)
				sum = sum + updateSum(child, difference, total);
		}
		long tmp = total-sum;
		long diff;
		if(sum > tmp)
			diff = sum - tmp;
		else
			diff = tmp - sum;
		if(difference.data > diff){
			difference.data = diff;
		}
		return sum;
	};
	
	public static class TreeNode{
		long data;
		List<TreeNode> childs;
		boolean visited;
		int vertexNumber;
		TreeNode(long data){
			this.data = data;
			childs = new ArrayList<>();
		}
		
		TreeNode(long diff, List<TreeNode> list) {
			this.data = diff;
		}
	}

}
