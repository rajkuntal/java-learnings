package com.raj.datastructure.hackerrank;
/*
 * A pair of nodes, , is a similar pair if both of the following conditions are true:

Node  is the ancestor of node 

Given a tree where each node is labeled from  to , can you find the number of similar pairs in the tree?

Input Format

The first line contains two space-separated integers,  (the number of nodes) and  (the similar pair qualifier), respectively. 
Each line  of the  subsequent lines contains two space-separated integers defining an edge connecting nodes  and , where node  is a parent to node .

Constraints


Output Format

Print a single integer denoting the number of similar pairs in the tree.

Sample Input

5 2
3 2
3 1
1 4
1 5
Sample Output

4
Explanation

The similar pairs are , , , and , so we print  as our answer. Observe that  and  are not similar pairs because they do not satisfy .

5 2 3 2 3 1 1 4 1 5
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class FindPairsInTree {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int k = sc.nextInt();
		int rootIndex = -1;
		TreeNode[] nodes = new TreeNode[n];
		for (int i = 0; i < n - 1; i++) {
			int n1 = sc.nextInt();
			int n2 = sc.nextInt();
			TreeNode node1 = nodes[n1 - 1];
			TreeNode node2 = nodes[n2 - 1];
			if (node1 == null) {
				nodes[n1 - 1] = new TreeNode(n1);
				node1 = nodes[n1 - 1];
			}
			if (node2 == null) {
				nodes[n2 - 1] = new TreeNode(n2);
				node2 = nodes[n2 - 1];
			}
			node1.childs.add(node2);
			if (i == 0) {
				rootIndex = n1 - 1;
			}
		}
		TreeNode diff = new TreeNode(0, null);
		findPairs(nodes[rootIndex], diff, k, "");
		System.out.println(diff.data);
	}

	public static void findPairs(TreeNode node, TreeNode difference, int k, String parents) {
		if (node == null)
			return;
		if (parents.isEmpty())
			parents = String.valueOf(node.data);
		else
			parents = parents + " " + String.valueOf(node.data);
		if (!parents.isEmpty()) {
			String[] parrentStringArr = parents.split(" ");
			long[] parentArr = new long[parrentStringArr.length];
			for(int h = 0; h < parrentStringArr.length; h++){
				parentArr[h] = Long.parseLong(parrentStringArr[h]);
			}
			Arrays.sort(parentArr);
			for (TreeNode child : node.childs) {
				long tmp = child.data + k;
				long tmp1 = child.data > k ? child.data-k : k-child.data;
				int index1 = findParentMaxIndexByBS(parentArr, tmp, false);
				int index2 = findParentMaxIndexByBS(parentArr, tmp1, true);
				if(index1 != -1 && index2 != -1){
					difference.data = difference.data + (index1-index2) + 1;
				}
				findPairs(child, difference, k, parents);
			}
		}
	}
	
	public static int findParentMaxIndexByBS(long[] parents, long tmp, boolean greater){
		int index = -1;
		int start = 0;
		int end = parents.length;
		int mid = (parents.length/2);
		while(start <= end && start < parents.length && end <= parents.length){
			if(!greater){
				if(parents[mid] <= tmp){
					index = mid;
					if(start == end)
						break;
					start = mid+1;
					mid = ((start + end)/2);
				} else{
					end = mid -1;
					mid = ((start + end)/2);
				}
			} else {
				if(parents[mid] < tmp){
					start = mid+1;
					mid = ((start + end)/2);
				} else {
					index = mid;
					if(start == end)
						break;
					end = mid -1;
					mid = ((start + end)/2);
				}
			}
		}
		return index;
	};

	public static class TreeNode {
		long data;
		List<TreeNode> childs;

		TreeNode(long data) {
			this.data = data;
			childs = new ArrayList<>();
		}
		TreeNode(long diff, List<TreeNode> list) {
			this.data = diff;
		}
	}

}
