package com.raj.datastructure.linkedlist;

public class TestLinkedList {
	
	public static void main(String[] args) {
		
		LinkListOperations listOperations = new LinkListOperations();
		listOperations.insertNode(2);
		listOperations.insertNode(10);
//		listOperations.insertNode(3);
//		listOperations.insertNode(4);
//		listOperations.insertNode(6);
		listOperations.printList();
		listOperations.deleteOdd();
		listOperations.printList();
		System.out.println("reverse link list with reverse method");
		listOperations.reverseReccursive();
		listOperations.printList();
		
		System.out.println("reverse link list with three pointer");
		listOperations.reverseLinkList();
		listOperations.printList();
		
		listOperations.deleteByKeyPosition(1);
		listOperations.printList();
		listOperations.deleteByKeyPosition(2);
		listOperations.printList();
		listOperations.deleteNodeByKey(9);
		listOperations.printList();
		listOperations.deleteNodeByKey(1);
		listOperations.printList();
		
		System.out.println("Change Binary Bit Link list to Decimal ");
		
		LinkListOperations listOperations1 = new LinkListOperations();
		listOperations1.insertNode(0);
		listOperations1.insertNode(0);
		listOperations1.insertNode(0);
		listOperations1.insertNode(1);
		System.out.println("First 1 Bit Node tail : "+listOperations1.tail.getData() + " - " + System.identityHashCode(listOperations1.tail));
		listOperations1.insertNode(1);
		listOperations1.insertNode(0);
		listOperations1.insertNode(0);
		listOperations1.insertNode(1);
		listOperations1.insertNode(0);
		listOperations1.reverseReccursive();
		// System.out.println("First 1 Bit Node is : "+listOperations1.firstOneBit.getData() + " - "+System.identityHashCode(listOperations1.firstOneBit));
		listOperations1.printList();
		System.out.println("ListOperations1 Decimal Conversion will be " + listOperations1.convertToDecimal() + " Short Method result ->"+listOperations1.decimalValue());
		
		LinkListOperations listOperations2 = new LinkListOperations();
		listOperations2.insertNode(0);
		listOperations2.insertNode(1);
		listOperations2.insertNode(0);
		listOperations.reverseReccursive();
		listOperations.printList();
		System.out.println("ListOperations2 Decimal Conversion will be " + listOperations2.convertToDecimal()+ " Short Method result ->"+listOperations2.decimalValue());
		
	}

}
