package com.raj.datastructure.linkedlist;

public class LinkListNode {
	
	private int data;
	private LinkListNode nextNode = null;
	
	public LinkListNode(int data) {
		// TODO Auto-generated constructor stub
		this.data = data;
	}
	
	public void setData(int data) {
		this.data = data;
	}
	
	public void setNextNode(LinkListNode nextNode) {
		this.nextNode = nextNode;
	}
	
	public int getData() {
		return this.data;
	}
	
	public LinkListNode getNextNode() {
		return this.nextNode;
	}
}
