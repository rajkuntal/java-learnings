package com.raj.datastructure.linkedlist;

import java.util.List;

import javax.management.ListenerNotFoundException;

public class LinkListOperations {
	
	private LinkListNode head;
	public LinkListNode tail;
	LinkListNode firstOneBit;
	
	public void insertNode(int data) {
		LinkListNode newNode = new LinkListNode(data);
		if(this.head == null) {
			this.head = newNode;
			this.tail = this.head;
		} else {
			this.tail.setNextNode(newNode);
			this.tail = newNode;
		}
	}
	
	public void deleteNodeByKey(int data) {
		LinkListNode prevNode;
		LinkListNode node;
		if(this.head == null) {
			System.out.println("list is empty");
		} else {
			prevNode = null;
			node = this.head;
			while(node != null) {
				if(node.getData() == data) {
					if(this.head == node) {
						this.head = node.getNextNode();
						System.out.println("Item is deleted - "+ node.getData());
						return;
					} else {
						prevNode.setNextNode(node.getNextNode());
						System.out.println("Item is deleted - "+ node.getData());
						return;
					}
				} else {
					prevNode = node;
					node = node.getNextNode();
				}
			}
		}
	}
	
	public void deleteByKeyPosition(int position) {
		LinkListNode prevNode;
		LinkListNode node;
		int indexCount = 2;
		if(this.head == null) {
			System.out.println("list is empty");
			return;
		} else if(position <= 0) {
			System.out.println("Given Position is zero");
		} else {
			prevNode = this.head;
			node = this.head;
			while(node != null) {
				if(position > 1) {
					if(position == indexCount) {
						prevNode.setNextNode(node.getNextNode());
						System.out.println("Node is deleted from index - " + position);
						return;
					} else {
						prevNode = node;
						node = node.getNextNode();
						indexCount++;
					}
				} else {
					this.head = node.getNextNode();
					System.out.println("Node is deleted from index - " + indexCount);
					return;
				}
			}
		}
	}
	
	public void reverseLinkList() {
		LinkListNode prev = null;
		LinkListNode current = this.head;
		LinkListNode next = null;
		while(current != null) {
			next = current.getNextNode();
			current.setNextNode(prev);
			prev= current;
			current = next;
		}
		this.head = prev;
	}
	
	public LinkListNode devideInParts(LinkListNode head) {
		if(head != null)
			return head.getNextNode();
		else
			return null;
	}
	
	public LinkListNode reverseReccursive(LinkListNode node) {
		if(node == null) {
			return null;
		}
		if(node.getNextNode() == null) {
			this.head = node;
			return node;
		}
		LinkListNode secElem = node.getNextNode();
		node.setNextNode(null);
		LinkListNode reverse = reverseReccursive(secElem);
		secElem.setNextNode(node);
		return reverse;
	}
	
	public void reverseReccursive() {
		reverseReccursive(this.head);
	}
	
	public int convertToDecimal() {
		LinkListNode node = this.head;
		int decimalBits = 0;
		int decimalValue = 0;
		if(firstOneBit == null || this.head == this.tail)
			decimalValue =  0;
		while(node != null) {
			if(node == this.head)
				decimalValue = 0;
			else if (node.getData() == 1)
				decimalValue = decimalValue + decimalBits;
			if(decimalBits == 0)
				decimalBits = 2;
			else
				decimalBits = decimalBits * 2;
			if(node == this.firstOneBit)
				break;
			node = node.getNextNode();
				
		}
		return decimalValue;
	}
	
	public int decimalValue() {
		int result = 0;
		LinkListNode node = this.head;
		while(node != null) {
			result = (result << 1) + node.getData();
			node = node.getNextNode();
		}
		return result;
	}
	
	public void printList() {
		LinkListNode node = this.head;
		if(node == null) 
			System.out.println("Nothing to print - List is empty");
		else {
			while(node != null) {
				System.out.print(node.getData() + "-->");
				node = node.getNextNode();
			}
		}
		System.out.println();
	}
	
	public LinkListNode deleteOdd() {
		LinkListNode list = this.head;
        System.out.println("Initail Head"+list.getData());
        LinkListNode head = list;
        LinkListNode prev = list;
        while(list != null){
            if(head.getData()%2 != 0){
                head = head.getNextNode();
                prev = head;
                list = head;
            }  else if(list != head &&list.getData()%2 != 0) {
                LinkListNode tmp = list.getNextNode();;
                prev.setNextNode(tmp);
                list = list.getNextNode();
            } else if (list != head  && list.getData()%2 == 0) {
                list = list.getNextNode();
                prev = list;
            } else {
                list = head.getNextNode();
                prev = head;
            }
        }
        System.out.println("head"+head.getData());
        return head;
    }
}
