package com.raj.datastructure.heap;

public class TestHeap {
	
	public static void main(String[] args) {
		Heap minHeap1 = new Heap(10);
		minHeap1.insertElementInMinHeap(8);
		minHeap1.insertElementInMinHeap(4);
		minHeap1.insertElementInMinHeap(2);
		minHeap1.insertElementInMinHeap(5);
		minHeap1.insertElementInMinHeap(10);
		minHeap1.insertElementInMinHeap(3);
		minHeap1.insertElementInMinHeap(9);
		minHeap1.printHeap();
		System.out.println();
		minHeap1.minHeapify(0);
		minHeap1.printHeap();
		System.out.println("Minimum Element is - "+minHeap1.getMinElementInMinHeap());
		System.out.println();
		System.out.println("Min Heapify the Randomaly Inserted array");
		Heap minHeap2 = new Heap(10);
		minHeap2.insertRandomElement(2);
		minHeap2.insertRandomElement(32);
		minHeap2.insertRandomElement(12);
		minHeap2.insertRandomElement(10);
		minHeap2.insertRandomElement(8);
		minHeap2.insertRandomElement(3);
		minHeap2.insertRandomElement(20);
		minHeap2.insertRandomElement(18);
		minHeap2.insertRandomElement(14);
		minHeap2.insertRandomElement(7);
		minHeap2.minHeapify(0);
		minHeap2.printHeap();
		System.out.println("Minimum Element is - "+minHeap2.getMinElementInMinHeap());
		System.out.println("Extract the minimum - " + minHeap2.extractMin());
		minHeap2.printHeap();
		
		// Max Heap Examples
		
		Heap maxHeap1 = new Heap(10);
		maxHeap1.insertInMaxHeap(8);
		maxHeap1.insertInMaxHeap(34);
		maxHeap1.insertInMaxHeap(86);
		maxHeap1.insertInMaxHeap(4);
		maxHeap1.insertInMaxHeap(23);
		maxHeap1.insertInMaxHeap(64);
		maxHeap1.insertInMaxHeap(35);
		maxHeap1.insertInMaxHeap(45);
		maxHeap1.insertInMaxHeap(78);
		maxHeap1.insertInMaxHeap(32);
		maxHeap1.insertInMaxHeap(17);
		maxHeap1.insertInMaxHeap(6);
		maxHeap1.printHeap();
		
		System.out.println();
		System.out.println("Max Heapify the Randomaly Inserted array");
		Heap maxHeap2 = new Heap(10);
		maxHeap2.insertRandomElement(2);
		maxHeap2.insertRandomElement(32);
		maxHeap2.insertRandomElement(12);
		maxHeap2.insertRandomElement(10);
		maxHeap2.insertRandomElement(8);
		maxHeap2.insertRandomElement(3);
		maxHeap2.insertRandomElement(20);
		maxHeap2.insertRandomElement(18);
		maxHeap2.insertRandomElement(14);
		maxHeap2.insertRandomElement(7);
		maxHeap2.maxHeapify(0);
		maxHeap2.printHeap();
		System.out.println("Max Heapify from last element");
		maxHeap2.maxHeapifyFromLastElement();
		
		System.out.println("Final Max Heapify");
		maxHeap2.printHeap();
		
		System.out.println();
		Heap heap1 = new Heap(6);
		heap1.insertRandomElement(6);
		heap1.insertRandomElement(8);
		heap1.insertRandomElement(4);
		heap1.insertRandomElement(5);
		heap1.insertRandomElement(2);
		heap1.insertRandomElement(3);
		System.out.println("Changes to min heap");
		heap1.minHeapify(0);
		heap1.maxHeapifyFromLastElement();
		System.out.println("Print the min heap");
		heap1.printHeap();
		StringBuilder num1 = new StringBuilder();
		StringBuilder num2 = new StringBuilder();
		for(int i = 0; i<=5 ; i=i+2) {
			int tmp = heap1.extractMin();
			num1.append((tmp == Integer.MIN_VALUE) ? "" : tmp);
			tmp = heap1.extractMin();
			num2.append((tmp == Integer.MIN_VALUE) ? "" : tmp);
		}
		System.out.println("Num1 - "+num1 + ": Num2 - "+num2);
		long sum = Long.parseLong(num1.toString()) + Long.parseLong(num2.toString());
		System.out.println("Sum of two lowest number in array "+ sum);
	}

}
