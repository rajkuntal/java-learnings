package com.raj.datastructure.heap;

public class Heap {
	
	private int[] heap;
	private int maxHeapSize = 0;
	private int heapSize = 0;
	private final int FRONT = 0;
	
	public Heap(int size) {
		this.maxHeapSize = size;
		this.heap = new int[size];
	}
	
	public boolean isLeaf(int pos) {
		if(pos > (this.maxHeapSize-1)/2 && pos < maxHeapSize-1)
			return true;
		return false;
	}
	
	public int parent(int current) {
		return (current-1)/2;
	}
	
	public int leftChild(int current) {
		return current*2+1;
	}
	
	public int rightChild(int current) {
		return (current*2)+2;
	}
	
	public void insertElementInMinHeap(int element) {
		if(heapSize == maxHeapSize)
			System.out.println("Min Heap is full");
		else {
			heap[heapSize] = element;
			int currentPos = heapSize;
			heapSize++;
			if(heapSize == 0) {
				System.out.println("Root Element inserted in Min Heap");
			} else {
				while(currentPos != 0) {
					if(heap[currentPos] < heap[parent(currentPos)]) {
						swapElements(currentPos, parent(currentPos));
						currentPos = parent(currentPos);
					} else
						break;
				}
			}
			
		}
	}
	
	public int getMinElementInMinHeap() {
		return heap[FRONT];
	}
	
	public void minHeapifyIfSubTreeIsAlreadyHeapify(int position) {
		
		int smallest = position;
		if(this.heapSize > leftChild(smallest) && heap[smallest] > heap[leftChild(smallest)]) {
			smallest = leftChild(smallest);
		}
		if(this.heapSize > rightChild(smallest) && heap[smallest] > heap[rightChild(smallest)]) {
			smallest = rightChild(smallest);
		}
		if(smallest != position) {
			swapElements(position, smallest);
			minHeapifyIfSubTreeIsAlreadyHeapify(smallest);
		}
	}
	
	public void minHeapifyByLastElement(int current) {
		if(current <= 0)
			return;
		if(!isLeaf(current)) {
			if(heap[current] < heap[leftChild(current)])
				minHeapify(leftChild(current));
		}
	}
	
	public void minHeapify(int current) {
		int position = current;
		if(current < heapSize) {
			if(leftChild(current) < heapSize) {
				minHeapify(leftChild(current));
			}
			if(rightChild(current) < heapSize) {
				minHeapify(rightChild(current));
			}
			while(position != 0) {
				if(heap[position] < heap[parent(position)]) {
					swapElements(position, parent(position));
					position = parent(position);
				} else
					break;
				
			}
		} 
	}
	
	public int extractMin() {
		int tmp;
		if(this.heapSize == 0)
			return Integer.MIN_VALUE;
		else {
			tmp = heap[FRONT];
			heap[FRONT] = heap[heapSize-1];
			heap[heapSize-1] = 0;
			heapSize--;
			minHeapify(FRONT);
			return tmp;
		}
	}
	
	// Max Heap methods
	
	public void insertInMaxHeap(int element) {
		if(this.heapSize == maxHeapSize-1) {
			System.out.println("Heap is Already full");
			return;
		}
		if(heapSize == 0) {
			heap[heapSize] = element;
			heapSize++;
		} else {
			heap[heapSize] = element;
			int current = heapSize;
			heapSize++;
			while(current != 0) {
				if(heap[current] > heap[parent(current)]) {
					swapElements(current, parent(current));
					current = parent(current);
				}
				else
					break;
			}
		}
	}
	
	public void maxHeapify(int current) {
		if(current < this.heapSize) {
			maxHeapify(leftChild(current));
			if(heap[current] > heap[parent(current)]){
				swapElements(current, parent(current));
			}
			maxHeapify(rightChild(current));
			if(heap[current] > heap[parent(current)]) {
				swapElements(current, parent(current));
			}
			
		}
	}
	
	public void maxHeapifyFromLastElement() {
		if(heapSize<=0){
			System.out.println("Heap is empty");
			return;
		}
		for(int j = FRONT; j<=(this.maxHeapSize-1)/2; j++) {
			for(int i = heapSize-1; i>FRONT; i--){
				if(heap[i] > heap[parent(i)])
					swapElements(i, parent(i));
			}
		}
	}
	
	public void insertRandomElement(int element) {
		if(heapSize == maxHeapSize)
			System.out.println("Heap is full");
		else {
			heap[heapSize] = element;
			heapSize++;
		}
	}
	
	public void swapElements(int pos1, int pos2) {
		int tmp = heap[pos1];
		heap[pos1] = heap[pos2];
		heap[pos2] = tmp;
	}
	
	public void printHeap() {
		int counter = 0;
		while(counter < heapSize) {
			if((leftChild(counter)) < heapSize)
				System.out.println("Element "+heap[counter]+" left child is "+heap[leftChild(counter)]);
			if((rightChild(counter)) < heapSize)
				System.out.println("Element "+heap[counter]+" Right child is "+heap[rightChild(counter)]);
			counter++;
		}
	}

}
