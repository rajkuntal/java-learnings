package com.raj.datastructure.heap;

import java.util.HashSet;
import java.util.Set;

public class Test2 {
	
	public static void main(String[] args) {
		String s = "He,had HAD:quite enough quite";
		
		System.out.println(firstRepeatedWord(s));
	}
		static String firstRepeatedWord(String s) {
			if(!s.isEmpty()) {
				s = s.replace(",", " ");
				s = s.replace(":", " ");
				s = s.replace(";", " ");
				s = s.replace("-", " ");
				s = s.replace(".", " ");
				String str = "";
				String[] arr = s.split(" ");
				Set<String> set = new HashSet<>();
				for(int i = 0; i<arr.length; i++) {
					str = arr[i].toLowerCase();
					if(!set.contains(str))
						set.add(str);
					else
						break;
				}
				return str;
			}
			return null;
	    }
}
