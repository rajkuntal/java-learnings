package com.raj.datastructure.heap;

import java.util.HashSet;
import java.util.Set;

public class Test {
	
	public static void main(String[] args) {
		int[] a = {0,0,10,10,20,20,30,10,10,40};
		System.out.println(marksForRank(a, 2));
	}
	
	static int marksForRank(int[] marks, int rank) {
		int[] sub = new int[rank];
		Set<Integer> s = new HashSet<>();
		if(marks.length > rank) {
			for(int i = 0; i<rank; i++){
				if(!s.contains(marks[i])) {
					sub[i] = marks[i];
					s.add(marks[i]);
				}
			}
			minHeapify(0, sub, sub.length);
			for(int i = rank; i< marks.length; i++) {
				if(marks[i] > sub[0] && !s.contains(marks[i])){
					s.remove(sub[0]);
					sub[0] = marks[i];
					s.add(marks[i]);
					minHeapify(0, sub, sub.length);
				}
			}
		}
		if(s.size() < rank)
			return 0;
		return sub[0];
    }

	
	public static void swapElements(int pos1, int pos2, int[] marks) {
		int tmp = marks[pos1];
		marks[pos1] = marks[pos2];
		marks[pos2] = tmp;
	}
	
	public static boolean isLeaf(int pos, int maxHeapSize) {
		if(pos > (maxHeapSize-1)/2 && pos < maxHeapSize-1)
			return true;
		return false;
	}
	
	public static int parent(int current) {
		return (current-1)/2;
	}
	
	public static int leftChild(int current) {
		return current*2+1;
	}
	
	public static int rightChild(int current) {
		return (current*2)+2;
	}
	
	public static void minHeapify(int current, int[] marks, int noOfStudents) {
		int position = current;
		if(current < noOfStudents) {
			if(leftChild(current) < noOfStudents) {
				minHeapify(leftChild(current), marks, noOfStudents);
			}
			if(rightChild(current) < noOfStudents) {
				minHeapify(rightChild(current), marks, noOfStudents);
			}
			while(position != 0) {
				if(marks[position] < marks[parent(position)]) {
					swapElements(position, parent(position), marks);
					position = parent(position);
				} else
					break;
				
			}
		} 
	}

}
