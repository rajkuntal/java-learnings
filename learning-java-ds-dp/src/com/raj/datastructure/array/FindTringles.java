package com.raj.datastructure.array;

import java.util.Arrays;

/**
 * 
 * 		   Count the number of possible triangles Given an unsorted array of
 *         positive integers. Find the number of triangles that can be formed
 *         with three different array elements as three sides of triangles. For
 *         a triangle to be possible from 3 values, the sum of any two values
 *         (or sides) must be greater than the third value (or third side). For
 *         example, if the input array is {4, 6, 3, 7}, the output should be 3.
 *         There are three triangles possible {3, 4, 6}, {4, 6, 7} and {3, 6,
 *         7}. Note that {3, 4, 7} is not a possible triangle. As another
 *         example, consider the array {10, 21, 22, 100, 101, 200, 300}. There
 *         can be 6 possible triangles: {10, 21, 22}, {21, 100, 101}, {22, 100,
 *         101}, {10, 100, 101}, {100, 101, 200} and {101, 200, 300}
 *
 */
public class FindTringles {
	
	public static void main(String[] args) {
		int[] arr = {10,20,30,40,50,60};
		Arrays.sort(arr);
		for(int i = 0; i < arr.length - 2; i++){
			for(int j = arr.length - 1; j >= i + 2; j--){
				int index = indexBinarySearch((arr[j] - arr[i]), i+1, j-1, arr);
				if(index > -1){
					for(int k = index; k < j ; k++){
						System.out.println(arr[i] + " "+ arr[k] +" "+arr[j]);
					}
				}
			}
		}
	}
	
	public static int indexBinarySearch(int value, int start , int last, int[] arr){
		int mid = (start+last)/2;
		int first = start;
		int index = -1;
		boolean backTrack = false;
		boolean forwordTrack = false;
		while(first <= last){
			if(first == last){
				if(arr[mid] > value)
					index = mid;
				break;
			}
			if(arr[mid] > value && !forwordTrack){
				backTrack = true;
				index = mid;
				last = mid-1;
				mid = mid/2;
			} else if(!backTrack){
				first = mid+1;
				mid = (mid+last)/2+1;
			} else
				break;
		}
		return index;
	}

}
