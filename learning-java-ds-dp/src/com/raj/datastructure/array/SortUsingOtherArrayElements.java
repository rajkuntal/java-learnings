package com.raj.datastructure.array;

/**
 * 
 * Given two arrays A1[] and A2[], sort A1 in such a way that the relative order
 * among the elements will be same as those are in A2. For the elements not
 * present in A2, append them at last in sorted order.
 * 
 * Input: A1[] = {2, 1, 2, 5, 7, 1, 9, 3, 6, 8, 8} A2[] = {2, 1, 8, 3} Output:
 * A1[] = {2, 2, 1, 1, 8, 8, 3, 5, 6, 7, 9} The code should handle all cases
 * like number of elements in A2[] may be more or less compared to A1[]. A2[]
 * may have some elements which may not be there in A1[] and vice versa is also
 * possible.
 *
 */

public class SortUsingOtherArrayElements {

	public static void main(String[] args) {
		int[] a1 = {2, 1, 2, 5, 7, 1, 9, 3, 6, 8, 8};
		int[] a2 = {2, 1, 8, 3};

		for (int i = 0, index = 0, j = 0; i < a2.length; i++) {
			j = index;
			while (j < a1.length) {
				if (a2[i] == a1[j]) {
					swapElements(a1, index, j);
					index++;
				}
				j++;
			}
		}
		for (int i = 0; i < a1.length; i++) {
			System.out.print(a1[i] + " ");
		}
	}

	public static void swapElements(int[] a, int i, int j) {
		int tmp = a[i];
		a[i] = a[j];
		a[j] = tmp;
	}

}
