package com.raj.datastructure.binarytree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 
 *Given Inorder Traversal of a Special Binary Tree in which key of every node is greater than keys in left and right children, 
 *construct the Binary Tree and return root.
 *
 */

public class SpecialBinaryTreeFronInOrder {
	
	public static void main(String[] args) {
		int[] inOrderArray = {1, 5, 10, 40, 30, 15, 28, 20};
		List<Integer> arr = new ArrayList<>();
		for(int i = 0; i < inOrderArray.length; i++){
			arr.add(inOrderArray[i]);
		}
		BTNode node = constructTree(arr);
		System.out.println("Root Node : "+ node.getData());
	}
	
	public static BTNode constructTree(List<Integer> l){
		if(l.size() == 0)
			return null;
		int max = Collections.max(l);
		List<Integer> leftTree = getSubList(l, max, true, false);
		List<Integer> rightTree = getSubList(l, max, false, true);
		BTNode node = new BTNode(max);
		node.setLeftChild(constructTree(leftTree));
		node.setRightChild(constructTree(rightTree));
		return node;
	}
	
	public static List<Integer> getSubList(List<Integer> l, int max ,boolean leftArray, boolean rightArray){
		List<Integer> subList = new ArrayList<>();
		int maxIndex = l.indexOf(max);
		if(leftArray){
			for(int i = 0; i < maxIndex; i++){
				subList.add(l.get(i));
			}
		}
		if(rightArray){
			for(int i = maxIndex + 1; i < l.size(); i++){
				subList.add(l.get(i));
			}
		}
		return subList;
	}
}
