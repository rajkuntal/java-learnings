package com.raj.datastructure.binarytree;

public interface BTOperations {
	
	public BTNode getRoot();
	
	public void setRoot(int data);
	
	public BTNode insertNodeInBT(int data);
	
	public void inOrderWithReccursion(BTNode node);
	
	public void preOrderWithReccursion(BTNode node);
	
	public void postOrderWithReccursion(BTNode node);
	
	public void preOrderWithoutRecursion(BTNode node);
	
	public void inOrderWithoutRecursion(BTNode node);
	
	public void postOrderWithoutRecursion(BTNode node);
	
	public void bstTraverse(BTNode node);
	
	public int treeDiameter(BTNode node);
	
	public int treeHeight(BTNode node);
	
	public int maxWidth(BTNode node);
	
	public void printNodeAtDistanceK(BTNode node, int k);
	
	public int sumRootToLeaf(BTNode node);
	
	public void printallLeafNodes(BTNode node);
	
	public void printPathFromRootToLeaf(BTNode node);
	

}
