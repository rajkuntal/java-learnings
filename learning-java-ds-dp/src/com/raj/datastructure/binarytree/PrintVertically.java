package com.raj.datastructure.binarytree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class PrintVertically {
	
	public static void main(String[] args) {
		Map<Integer, List<Integer>> m = new TreeMap<>();
		BTNode node = new BTNode(1);
		node.setLeftChild(new BTNode(2));
		node.setRightChild(new BTNode(3));
		node.getLeftChild().setLeftChild(new BTNode(4));
		node.getLeftChild().setRightChild(new BTNode(5));
		node.getRightChild().setLeftChild(new BTNode(6));
		node.getRightChild().setRightChild(new BTNode(7));
		node.getRightChild().getLeftChild().setRightChild(new BTNode(8));
		node.getRightChild().getRightChild().setRightChild(new BTNode(9));
		
		m = printVertical(node, m, 0);
		Set<Integer> entries = m.keySet();
		for(int e : entries){
			List<Integer> ll = m.get(e);
			System.out.println(ll);
		}
	}
	
	public static Map<Integer, List<Integer>> printVertical(BTNode node, Map<Integer, List<Integer>> m, int pos){
		List<Integer> ll;
		if(node == null)
			return m;
		if(!m.containsKey(pos))
			ll = new ArrayList<>();
		else
			ll = m.get(pos);
		ll.add(node.getData());
		m.put(pos, ll);
		printVertical(node.getLeftChild(), m, pos-1);
		printVertical(node.getRightChild(), m, pos+1);
		return m;
	}

}
