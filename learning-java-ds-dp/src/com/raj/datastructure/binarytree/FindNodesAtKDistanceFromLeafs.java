package com.raj.datastructure.binarytree;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @author Raj
 * Given a Binary Tree and a positive integer k, 
 * print all nodes that are distance k from a leaf node.
 *
 */
public class FindNodesAtKDistanceFromLeafs {
	
	public static void main(String[] args) {
		BTNode node = new BTNode(1);
		node.setLeftChild(new BTNode(2));
		node.setRightChild(new BTNode(3));
		node.getLeftChild().setLeftChild(new BTNode(4));
		node.getLeftChild().setRightChild(new BTNode(5));
		node.getRightChild().setLeftChild(new BTNode(6));
		node.getRightChild().setRightChild(new BTNode(7));
		node.getRightChild().getLeftChild().setRightChild(new BTNode(8));
		Map<Integer, Integer> map = new HashMap<>();
		printKDistanceNodes(node, 2, map);
		Set<Integer> keys = map.keySet();
		System.out.println();
		for(int k : keys){
			System.out.println(k + "-->"+map.get(k));
		}
	}
	
	public static int findDistance(BTNode node){
		if(node == null)
			return 0;
		if(node.getLeftChild() == null && node.getRightChild() == null)
			return 0;
		return 1+Math.max(findDistance(node.getLeftChild()), findDistance(node.getRightChild()));
	}
	
	public static void printKDistanceNodes(BTNode node, int k, Map<Integer, Integer> map){
		if(node != null){
			map.put(node.getData(), findDistance(node));
			if(findDistance(node) == k){
				System.out.print(node.getData() + ", ");
			}
			printKDistanceNodes(node.getLeftChild(), k, map);
			printKDistanceNodes(node.getRightChild(), k, map);
		}
			
	}

}
