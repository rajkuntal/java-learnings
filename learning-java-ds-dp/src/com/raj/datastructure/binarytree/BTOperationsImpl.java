package com.raj.datastructure.binarytree;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class BTOperationsImpl implements BTOperations {
	
	public BTNode root;
	
	public BTNode getRoot() {
		return this.root;
	}
	
	public void setRoot(int data) {
		this.root = new BTNode(data);
	}
	
	@Override
	public BTNode insertNodeInBT(int data) {
		BTNode newNode = new BTNode(data);
		if(this.root == null) {
			this.root = newNode;
			return this.root;
		}
		else {
			Queue<BTNode> q = new LinkedList<BTNode>();
			BTNode node = null;
			q.offer(this.root);
			while(!q.isEmpty()) {
				node = q.poll();
				if(node.getLeftChild() == null){
					node.setLeftChild(newNode);
					return newNode;
				}
				else
					q.offer(node.getLeftChild());
				if(node.getRightChild() == null) {
					node.setRightChild(newNode);
					return newNode;
				}
				else
					q.offer(node.getRightChild());
			}
		}
		return newNode;
	}

	@Override
	public void preOrderWithReccursion(BTNode node) {
		if(node != null) {
			System.out.print(node.getData() + " ");
			preOrderWithReccursion(node.getLeftChild());
			preOrderWithReccursion(node.getRightChild());
		}
	}

	@Override
	public void inOrderWithReccursion(BTNode node) {
		if(node != null) {
			inOrderWithReccursion(node.getLeftChild());
			System.out.print(node.getData() + " ");
			inOrderWithReccursion(node.getRightChild());
		}
	}

	@Override
	public void postOrderWithReccursion(BTNode node) {
		if(node != null) {
			postOrderWithReccursion(node.getLeftChild());
			postOrderWithReccursion(node.getRightChild());
			System.out.print(node.getData() + " ");
		}		
	}

	@Override
	public void inOrderWithoutRecursion(BTNode node) {
		if(node != null){
			Stack<BTNode> stack = new Stack<BTNode>();
			stack.push(node);
			while(!stack.isEmpty()) {
				while(node != null) {
					if(node.getLeftChild() != null) {
						stack.push(node.getLeftChild());
					}
					node = node.getLeftChild();
				}
				node = stack.pop();
				System.out.print(node.getData() + " ");
				if(node.getRightChild() != null) {
					stack.push(node.getRightChild());
					node = node.getRightChild();
				}
			}
		}
	}

	@Override
	public void preOrderWithoutRecursion(BTNode node) {
		if(node != null) {
			Stack<BTNode> stack = new Stack<>();
			Queue<BTNode> queue = new LinkedList<>();
			stack.push(node);
			while(!stack.isEmpty()) {
				node = stack.pop();
				queue.offer(node);
				if(node.getRightChild() != null) {
					stack.push(node.getRightChild());
				}
				if(node.getLeftChild() != null) {
					stack.push(node.getLeftChild());
				}
			}
			while(!queue.isEmpty()) {
				System.out.print(queue.poll().getData()+" ");
			}
		}
	}

	@Override
	public void postOrderWithoutRecursion(BTNode node) {
		
	}

	@Override
	public void bstTraverse(BTNode node) {
		if(node != null) {
			Queue<BTNode> queue = new LinkedList<>();
			queue.offer(node);
			while(!queue.isEmpty()) {
				node = queue.poll();
				System.out.print(node.getData() +" ");
				if(node.getLeftChild() != null)
					queue.offer(node.getLeftChild());
				if(node.getRightChild() != null)
					queue.offer(node.getRightChild());
			}
		}
		
	}
	

	@Override
	public int treeDiameter(BTNode node) {
		int leftDiameter = 0;
		int rightDiameter = 0;
		if(node != null) {
			leftDiameter++;
			treeDiameter(node.getLeftChild());
			rightDiameter++;
			treeDiameter(node.getRightChild());
		}
		return (leftDiameter+rightDiameter);
	}

	@Override
	public int treeHeight(BTNode node) {
		int leftTreeHeight = 0;
		int rightTreeHeight = 0;
		int treeHeight = 0;
		if(node != null){
			leftTreeHeight = (treeHeight(node.getLeftChild()));
			rightTreeHeight =(treeHeight(node.getRightChild()));
			treeHeight = (leftTreeHeight > rightTreeHeight ? leftTreeHeight+1 : rightTreeHeight+1);
			return treeHeight;
		}
		return 0;
	}
	
	
	public int maxWidth(BTNode node) {
		int width = 0;
		if(node != null) {
			Queue<BTNode> q = new LinkedList<>();
			q.offer(node);
			int count = 0;
			while(!q.isEmpty()) {
				count = q.size();
				width = (count > width ? count : width);
				for(int i = 0; i < count; i++) {
					node = q.poll();
					if(node.getLeftChild() != null)
						q.offer(node.getLeftChild());
					if(node.getRightChild() != null)
						q.offer(node.getRightChild());
				}
			}
		}
		return width;
	}

	// wrong
	@Override
	public void printNodeAtDistanceK(BTNode node,int k) {
		Queue<BTNode> queue = new LinkedList<>();
		int level = 0;
		if(node != null){
			queue.offer(node);
			while(!queue.isEmpty() && level < k) {
				int size = queue.size();
				for(int i=0; i<size; i++){
					node = queue.poll();
					if(node.getLeftChild() != null)
						queue.offer(node.getLeftChild());
					if(node.getRightChild() != null)
						queue.offer(node.getRightChild());
				}
				level++;
			}
			if(queue.isEmpty())
				System.out.println("There is no element at distance : "+k);
			else {
				while(!queue.isEmpty()) {
					System.out.print(queue.poll().getData() +" ");
				}
			}
		}
	}

	// wrong
	@Override
	public int sumRootToLeaf(BTNode node) {
		int leftdata = 0;
		int data = 0;
		if(node != null) {
			leftdata = node.getData() + sumRootToLeaf(node.getLeftChild());
			data = leftdata;
			System.out.print(data + " ");
			return data;
		}
		return 0;
	}

	@Override
	public void printallLeafNodes(BTNode node) {
		if(node != null) {
			if(node.getLeftChild() == null && node.getRightChild() ==null)
				System.out.print(node.getData() + " ");
			printallLeafNodes(node.getLeftChild());
			printallLeafNodes(node.getRightChild());
		}
	}

	@Override
	public void printPathFromRootToLeaf(BTNode node) {
		if(node != null) {
			Queue<BTNode> q = new LinkedList<>();
			BTNode recentNode = node;
			q.offer(node);
			while(!q.isEmpty()) {
				node = recentNode;
				if(node.getLeftChild() == null && node.getRightChild() == null) {
					int qSize = q.size();
					for(int i = 0 ; i < qSize; i++){
						node = q.poll();
						System.out.print(node.getData() + "-->");
						q.offer(node);
						recentNode = node;
					}
					System.out.println();
				}
				node = q.peek();
				if(node.getLeftChild() != null) {
					node = node.getLeftChild();
					while(node != null) {
						q.offer(node);
						recentNode = node;
						node = node.getLeftChild();
					}
				} else {
					node = node.getRightChild();
					while(node != null) {
						q.offer(node);
						recentNode = node;
						node = node.getRightChild();
					}
				}
					
			}
		}
	}
	
}
