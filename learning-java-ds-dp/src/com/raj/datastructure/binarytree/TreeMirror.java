package com.raj.datastructure.binarytree;

public class TreeMirror {
	
	public static void main(String[] args) {
		BTNode node = new BTNode(1);
		node.setLeftChild(new BTNode(2));
		node.setRightChild(new BTNode(3));
		node.getLeftChild().setLeftChild(new BTNode(4));
		node.getLeftChild().setRightChild(new BTNode(5));
		node.getRightChild().setLeftChild(new BTNode(6));
		node.getRightChild().setRightChild(new BTNode(7));
		node.getRightChild().getLeftChild().setRightChild(new BTNode(8));
		node.getRightChild().getRightChild().setRightChild(new BTNode(9));
		node = getMirror(node);
		System.out.println(node.getData());
	}
	
	public static BTNode getMirror(BTNode node){
		if(node == null)
			return null;
		BTNode tmp = new BTNode();
		getMirror(node.getLeftChild());
		getMirror(node.getRightChild());
		tmp = node.getLeftChild();
		node.setLeftChild(node.getRightChild());
		node.setRightChild(tmp);
		return node;
	}

}
