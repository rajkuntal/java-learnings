package com.raj.datastructure.binarytree;

import java.util.ArrayList;
import java.util.List;

public class PrintAllRootToLeafPath {
	
	public static void main(String[] args) {
		
		BTNode node = new BTNode(1);
		node.setLeftChild(new BTNode(2));
		node.setRightChild(new BTNode(3));
		node.getLeftChild().setLeftChild(new BTNode(4));
		node.getLeftChild().setRightChild(new BTNode(5));
		node.getRightChild().setLeftChild(new BTNode(6));
		node.getRightChild().setRightChild(new BTNode(7));
		node.getRightChild().getLeftChild().setRightChild(new BTNode(8));
		node.getRightChild().getRightChild().setRightChild(new BTNode(9));
		printRootToLeafPaths(node, null);
	}
	
	public static void printRootToLeafPaths(BTNode node, List<Integer> ll){
		if(ll == null)
			ll = new ArrayList<>();
		if(node != null){
			ll.add(node.getData());
			if(node != null && node.getLeftChild() == null && node.getRightChild() == null){
				System.out.println(ll);
			}
			List<Integer> leftList = new ArrayList<>();
			leftList.addAll(ll);
			printRootToLeafPaths(node.getLeftChild(), leftList);
			
			List<Integer> rightList = new ArrayList<>();
			rightList.addAll(ll);
			printRootToLeafPaths(node.getRightChild(), rightList);
		}
		
	}

}
