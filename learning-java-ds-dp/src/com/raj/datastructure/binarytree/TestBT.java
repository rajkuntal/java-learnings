package com.raj.datastructure.binarytree;

public class TestBT {
	
	public static void main(String[] args) {
		
		BTOperations bt1 = new BTOperationsImpl();
		bt1.insertNodeInBT(4);
		bt1.insertNodeInBT(6);
		bt1.insertNodeInBT(10);
		bt1.insertNodeInBT(3);
		bt1.insertNodeInBT(8);
		bt1.insertNodeInBT(5);
		
		System.out.println("Pre Order Tree Traversal with recurrsion");
		bt1.preOrderWithReccursion(bt1.getRoot());
		System.out.println();
		
		System.out.println("Pre Order Tree Traversal with-out recurrsion");
		bt1.preOrderWithoutRecursion(bt1.getRoot());
		System.out.println();
		
		System.out.println("In Order Tree Traversal with recurrsion");
		bt1.inOrderWithReccursion(bt1.getRoot());
		System.out.println();
		
		System.out.println("In Order Tree Traversal with-out recurrsion");
		bt1.inOrderWithoutRecursion(bt1.getRoot());
		System.out.println();
		
		System.out.println("Post Order Tree Traversal with recurrsion");
		bt1.postOrderWithReccursion(bt1.getRoot());
		System.out.println();
		
		System.out.println("BST or Level Order traversal");
		bt1.bstTraverse(bt1.getRoot());
		System.out.println();
		
		System.out.println("Tree Maximum height = "+bt1.treeHeight(bt1.getRoot()));
		
		System.out.println("Tree maximum width = "+bt1.maxWidth(bt1.getRoot()));
		
		int k=1;
		System.out.println("Elements at "+k+" position from root");
		bt1.printNodeAtDistanceK(bt1.getRoot(), 1);
		
		System.out.println("Sum from root to leaf");
		System.out.println("Sum from root to leaf :" + bt1.sumRootToLeaf(bt1.getRoot()));
		
		System.out.println("Print all Leaf nodes");
		bt1.printallLeafNodes(bt1.getRoot());
		
		System.out.println();
		System.out.println("Print Path from Root to leaf");
		bt1.printPathFromRootToLeaf(bt1.getRoot());
	}

}
