package com.raj.datastructure.binarytree;

public class BTNode {
	
	private int data;
	private BTNode leftChild = null;
	private BTNode rightChild = null;
	
	public BTNode(){
		
	}
	
	public BTNode(int data) {
		this.data = data;
		this.leftChild = null;
		this.rightChild = null;
	}

	public int getData() {
		return data;
	}

	public void setData(int data) {
		this.data = data;
	}

	public BTNode getLeftChild() {
		return this.leftChild;
	}

	public void setLeftChild(BTNode leftChild) {
		this.leftChild = leftChild;
	}

	public BTNode getRightChild() {
		return this.rightChild;
	}

	public void setRightChild(BTNode rightChild) {
		this.rightChild = rightChild;
	}
	
}
