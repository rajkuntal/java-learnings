package com.raj.datastructure.tries;

import java.util.List;

public interface TrieOperations {
	
	public TrieNode root = null;
	
	public void insertIntoTreis(String key);
	
	public void searchInTries(String key);
	
	public List<String> searchTopElements(int top, String word);
	
	public void printTreis();
	

}
