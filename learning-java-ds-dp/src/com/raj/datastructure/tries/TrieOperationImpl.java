package com.raj.datastructure.tries;

import java.util.ArrayList;
import java.util.List;

public class TrieOperationImpl implements TrieOperations {
	
	public TrieNode root = null;
	
	public TrieOperationImpl() {
		this.root = null;
	}

	@Override
	public void insertIntoTreis(String word) {
		if(this.root == null) {
			this.root = new TrieNode();
		}
		
		if(word.length() > 0) {
			char[] keys = word.toCharArray();
			TrieNode node = this.root;
			TrieNode newNode = null;
			for(int i = 0; i < word.length(); i++) {
				if(node.getChildNodeByKey(keys[i]) != null)
					node = node.getChildNodeByKey(keys[i]);
				else {
					newNode = new TrieNode(keys[i]);
					if(i == word.length()-1)
						node.addChildNode(newNode, true);
					else
						node.addChildNode(newNode, false);
					node = newNode;
				}
			}
		}
	}

	@Override
	public void searchInTries(String word) {
		TrieNode node = this.root;
		if(node != null) {
			char[] keys = word.toCharArray();
			for(int i=0; i< keys.length; i++) {
				if(i == keys.length-1) {
					node = node.getChildNodeByKey(keys[i]);
					if(node != null) {
						if(node.isLeaf())
							System.out.println(word+" is available in Tries");
						else
							System.out.println(word+" is not available in Tries");
					} else {
						System.out.println(word+" is not available in Tries");
						return;
					}
				} else {
					node = node.getChildNodeByKey(keys[i]);
					if(node == null) {
						System.out.println(word+" is not available in Tries");
						return;
					}
				}
			}
		}
	}

	@Override
	public List<String> searchTopElements(int top, String word) {
		TrieNode node = null;
		List<String> elements = new ArrayList<>();
		if(node != null) {
			for(int i = 0; i < word.length(); i++) {
				
			}
		}
		return null;
	}

	public String printTreis(TrieNode node, String sb) {
		if(node == null) return sb;
		
		for(int i = 0; i<26; i++){
			TrieNode nextNode = node.getChildNodes()[i];
			if(nextNode != null){
				sb =  sb + nextNode.getKey();
				if(nextNode.isLeaf()) {
					System.out.println(sb.toString());
				}
				
				if(nextNode.getChildNodes()[0] == null) {
					return sb;
				}
				sb = printTreis(nextNode, sb);
			} else {
				break;
			}
		}
		if(sb.length() > 1)
			sb = sb.substring(0, sb.length()-2);
		else 
			sb = "";
		return sb;
	}
	
	@Override
	public void printTreis(){
		printTreis(root, "");
	}
}
