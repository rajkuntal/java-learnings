package com.raj.datastructure.tries;

public class TestTries {
	
	public static void main(String[] args) {
		TrieOperations ops1 = new TrieOperationImpl();
		ops1.insertIntoTreis("hello");
		ops1.insertIntoTreis("hollow");
		ops1.insertIntoTreis("help");
		ops1.insertIntoTreis("helper");
		ops1.insertIntoTreis("array");
		ops1.insertIntoTreis("arey");
		ops1.printTreis();
	}

}
