package com.raj.datastructure.tries;

public class TrieNode {
	
	private TrieNode[] childNodes;
	private char key;
	private boolean isLeaf;
	private int noOfChildNodes;
	
	public TrieNode() {
		this.childNodes = new TrieNode[26];
		this.isLeaf = false;
	}
	
	public TrieNode(char key) {
		this.key = key;
		this.childNodes = new TrieNode[26];
		this.isLeaf = false;
	}

	public TrieNode[] getChildNodes() {
		return childNodes;
	}
	
	public TrieNode getChildNodeByKey(char key) {
		TrieNode childNode = null;
		for(int i=0; i< this.noOfChildNodes; i++) {
			if(this.childNodes[i].key == key) {
				childNode = this.childNodes[i];
				return childNode;
			}
		}
		return childNode;
	}

	public void setChildNodes(TrieNode[] childNodes) {
		this.childNodes = childNodes;
	}
	
	public void addChildNode(TrieNode childNode,boolean isLeaf) {
		if(childNode != null) {
			childNode.isLeaf = isLeaf;
			this.childNodes[noOfChildNodes] = childNode;
			noOfChildNodes++;
		}
	}

	public char getKey() {
		return key;
	}

	public void setKey(char key) {
		this.key = key;
	}

	public boolean isLeaf() {
		return isLeaf;
	}

	public void setLeaf(boolean isLeaf) {
		this.isLeaf = isLeaf;
	}

	public int getNoOfChildNodes() {
		return noOfChildNodes;
	}

	public void setNoOfChildNodes(int noOfChildNodes) {
		this.noOfChildNodes = noOfChildNodes;
	}
	
	
}
