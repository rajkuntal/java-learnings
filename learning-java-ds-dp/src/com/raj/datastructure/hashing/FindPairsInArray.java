package com.raj.datastructure.hashing;

import java.util.*;
import java.util.Map.Entry;

public class FindPairsInArray {
	
	public static void main (String[] args) {
		int[] a = {91, 74, 66, 48};
		int k = 10;
		if(checkPairs(a,k))
		    System.out.println("Found Pairs");
		else
		    System.out.println("Pairs not found");
		int m =6; 
		int l =7;
		double i = m+l;
		double j = 3;
		
		double d = (double) (i/j);
		System.out.println(d);
		System.out.println((int)Math.ceil(d));
		
		System.out.println("Sequence:"+appearanceCount(4,11,"cAdaA","AbrAAcadbRa"));
	}
	
	public static boolean checkPairs(int[] a, int k) {
	    boolean d = false;
	    int key = 0;
		Map<Integer, Boolean> map = new HashMap<>();
		if(a.length %2 != 0)
		    return false;
		for(int i = 0; i<a.length; i++) {
	        Set<Entry<Integer, Boolean>> entries = map.entrySet();
	        d = false;
	        key = 0;
	        for(Entry<Integer, Boolean> e : entries){
	            if(!e.getValue()){
	                if((e.getKey()+a[i])%k == 0){
	                    d = true;
	                    key = e.getKey();
	                    break;
	                }
	            }
	        }
	        if(!d)
	        	map.put(a[i], false);
	        else{
	        	map.put(key, true);
                map.put(a[i], true);
	        }
		}
		d = false;
		Set<Entry<Integer, Boolean>> entries1 = map.entrySet();
		for(Entry<Integer, Boolean> e : entries1){
			System.out.println("key:"+e.getKey()+"  value:"+e.getValue());
		    if(e.getValue()){
		        d = true;
		    } else
		    	return false;
		}
		return d;
	}
	
    public static int appearanceCount(int input1,int input2, String input3, String input4)
    {
	    char[] word = input3.toCharArray();
	    char[] seq = input4.toCharArray();
	    int posSeq = 0;
	    Map<Character, Integer> map = new HashMap<>();
	    if(input1 > 0){
	        for(int i = 0; i < input1; i++){
	            if(!map.containsKey(word[i]))
	                map.put(word[i], 1);
	            else
	                map.put(word[i], map.get(word[i])+1);
	        }
	    }
	    for(int j = 0; j < input2; j++){
	        Map<Character, Integer> m = new HashMap<>();
	        boolean found = false;
	        int p = 0;
	        if(map.containsKey(seq[j])){
	        	if(input2-j>input1){
		            for(int k = j; k < input1+j; k++){
		                if(map.containsKey(seq[k])){
		                	p = p+1;
		                    int count = map.get(seq[k]);
		                    if(!m.containsKey(seq[k])){
		                        m.put(seq[k], 1);
		                        found = true;
		                    }
		                    else if(m.containsKey(seq[k]) && count < m.get(seq[k])){
		                        m.put(seq[k], m.get(seq[k])+1);
		                        found = true;
		                    } else {
		                        found = false;
		                        break;
		                    }
		                } else {
		                    found = false;
		                    break;
		                }
		            }
		        }
	        }
	        if(found && p == input1)
	            posSeq = posSeq +1;
	    }
	    return posSeq;
    }


}
